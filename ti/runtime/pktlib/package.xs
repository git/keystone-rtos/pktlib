/*
 *  ======== package.xs ========
 *
 */


/*
 *  ======== Package.getLibs ========
 *  This function is called when a program's configuration files are
 *  being generated and it returns the name of a library appropriate
 *  for the program's configuration.
 */

function getLibs(prog)
{
    var lib = "";
    var suffix = prog.build.target.suffix; 
    var name = this.$name + ".a" + suffix; 
    

    /* Detect the platform type using XDCPATH and 
       set platformType to "EVM6670" or "EVM6614" */
    var xdcPath = environment["XDCPATH"];
    var platform = xdcPath.indexOf("tci6614");
    if (platform == -1)
        var platformType = "EVM6670";
    else
        var platformType = "EVM6614";
    
    libtemp = lib + "lib/" + platformType;
    if (java.io.File(this.packageBase + libtemp).exists())
        lib = lib + "lib/" + platformType + "/" + name;
    else
        lib = lib + "lib/" + name;
        
    if (java.io.File(this.packageBase + lib).exists()) {
        return lib;
    }

    /* could not find any library, throw exception */
    throw Error("Library not found: " + name);
}

/*
 *  ======== package.close ========
 */
function close()
{    
    if (xdc.om.$name != 'cfg') {
        return;
    }
}


