
Packet Lib Module Unit Test
---------------------------
The Packet Library Unit Test code is written to test the various API provided by the Packet Library.
Following are the list of test cases covered:
- Create and find heap by name.
- Allocate packets.
- Packet Merge
- Packet Clone
- Packet Split
- Multiple Heaps: Operate on two heaps and clone the packets
- Free packets
- Super Heaps
- Thresholds
- Starvation Heaps
- Shared Heaps across multiple cores.Default number of cores is 2

Upon Successful completion following log is expected:
[C66xx_1] Debug: Shared Heap Test Passed.

and 

[C66xx_0] Debug: All tests passed