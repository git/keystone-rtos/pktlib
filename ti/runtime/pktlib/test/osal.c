/**
 *   @file  osal.c
 *
 *   @brief   
 *      This is the OS abstraction layer for the CPPI and QMSS.
 *
 *  \par
 *  ============================================================================
 *  @n   (C) Copyright 2012, Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <xdc/std.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include <ti/sysbios/family/c64p/Hwi.h>
#include <ti/sysbios/family/c64p/EventCombiner.h>
#include <ti/sysbios/family/c66/tci66xx/CpIntc.h>

/* CSL Semaphore module includes */
#include <ti/csl/csl_semAux.h>

/* CSL Cache Include Files. */
#include <ti/csl/csl_cache.h>
#include <ti/csl/csl_xmcAux.h>
#include <ti/csl/csl_cacheAux.h>

/* IPC includes */ 
#include <ti/ipc/GateMP.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/ListMP.h>
#include <ti/ipc/SharedRegion.h>

/* Packet Library Include File */
#include <ti/runtime/pktlib/pktlib.h>

/**********************************************************************
 ****************************** Defines *******************************
 **********************************************************************/
#define CPPI_HW_SEM             1
#define QMSS_HW_SEM             2
#define NAMED_RES_HW_SEM        6

#define OSAL_POOL_SIZE      8192

/**********************************************************************
 ************************** Global Variables **************************
 **********************************************************************/
uint32_t              qmssMallocCounter = 0;
uint32_t              qmssFreeCounter   = 0;
uint32_t              cppiMallocCounter = 0;
uint32_t              cppiFreeCounter   = 0;

uint8_t               globalMemoryPool[OSAL_POOL_SIZE];
uint32_t              index = 0;


/**********************************************************************
 ************************** Global Variables **************************
 **********************************************************************/
uint32_t rmMallocCounter = 0;
uint32_t rmFreeCounter   = 0;

/**********************************************************************
 *************************** OSAL Functions **************************
 **********************************************************************/

/* FUNCTION PURPOSE: Allocates memory
 ***********************************************************************
 * DESCRIPTION: The function is used to allocate a memory block of the
 *              specified size.
 */
void* Osal_rmMalloc (uint32_t num_bytes)
{
	Error_Block	errorBlock;

    /* Increment the allocation counter. */
    rmMallocCounter++;

	/* Allocate memory. */
	return Memory_alloc(NULL, num_bytes, 0, &errorBlock);
}

/* FUNCTION PURPOSE: Frees memory
 ***********************************************************************
 * DESCRIPTION: The function is used to free a memory block of the
 *              specified size.
 */ 
void Osal_rmFree (void *ptr, uint32_t size)
{
    /* Increment the free counter. */
    rmFreeCounter++;
	Memory_free(NULL, ptr, size);
}

/**********************************************************************
 *************************** OSAL Functions **************************
 **********************************************************************/

/**
 *  @b Description
 *  @n  
 *      The function is used to allocate a memory block of the specified size.
 *
 *  @param[in]  num_bytes
 *      Number of bytes to be allocated.
 *
 *  @retval
 *      Allocated block address
 */
void* Osal_qmssMalloc (uint32_t num_bytes)
{
    /* Increment the allocation counter. */
    qmssMallocCounter++;
    return NULL;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to free a memory block of the specified size.
 *
 *  @param[in]  ptr
 *      Pointer to the memory block to be cleaned up.
 *
 *  @param[in]  size
 *      Size of the memory block to be cleaned up.
 *
 *  @retval
 *      Not Applicable
 */
void Osal_qmssFree (void *ptr, uint32_t size)
{
    /* Increment the free counter. */
    qmssFreeCounter++;	
}

/**
 *  @b Description
 *  @n  
 *      The function is used to enter a critical section.
 *      Function protects against 
 *      
 *      access from multiple cores 
 *      and 
 *      access from multiple threads on single core
 *
 *  @retval
 *      Handle used to lock critical section
 */
void* Osal_qmssCsEnter (void)
{
    /* Get the hardware semaphore */
    while ((CSL_semAcquireDirect (QMSS_HW_SEM)) == 0);

    /* Create Semaphore for protection against access from multiple threads 
     * Not created here becasue application is not multithreaded */
    return NULL;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to exit a critical section 
 *      protected using Osal_qmssCsEnter() API.
 *
 *  @param[in]  CsHandle
 *      Handle for unlocking critical section.
 *
 *  @retval
 *      Not Applicable
 */
void Osal_qmssCsExit (void *CsHandle)
{
    /* Release the hardware semaphore */ 
    CSL_semReleaseSemaphore (QMSS_HW_SEM);
    return;
}
/**
 *  @b Description
 *  @n  
 *      The function is used to enter a critical section.
 *      Function protects against 
 *      access from multiple threads on single core
 *
 *  @retval
 *      Handle used to lock critical section
 */
void* Osal_qmssMtCsEnter (void)
{
    /* Create Semaphore for protection against access from multiple threads 
     * Not created here becasue application is not multithreaded */
    return NULL;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to exit a critical section
 *      protected using Osal_qmssMtCsEnter() API.
 *
 *  @param[in]  CsHandle
 *      Handle for unlocking critical section.
 *
 *  @retval
 *      Not Applicable
 */
void Osal_qmssMtCsExit (void *CsHandle)
{
    /* Release Semaphore using handle */
        
    return;
}

/**
 *  @b Description
 *  @n  
 *      The function is the QMSS OSAL Logging API which logs 
 *      the messages on the console.
 *
 *  @param[in]  fmt
 *      Formatted String.
 *
 *  @retval
 *      Not Applicable
 */
void Osal_qmssLog ( char *fmt, ... )
{
}

/**
 *  @b Description
 *  @n  
 *      The function is used to indicate that a block of memory is
 *      about to be accessed. If the memory block is cached then this
 *      indicates that the application would need to ensure that the
 *      cache is updated with the data from the actual memory.
 *
 *  @param[in]  ptr
 *       Address of memory block
 *  @param[in]  size
 *       Size of memory block
 *  @param[in]  checkAlign
 *       Flags that enables checks for address cache line alignment
 *
 *  @retval
 *      Not Applicable
 */
static void Osal_beginMemAccess (void *ptr, uint32_t size, uint32_t checkAlign)
{
	uint32_t  key;

#ifdef CACHE_ALIGNMENT_CHECK
    /* Validate the address only if required to do so. */
    if (checkAlign == 1)
    {
		/* Check if DDR3 address is 128 byte aligned */
		if ((((int)ptr & 0x80000000) == 0x80000000) && (((int)ptr % 0x80) != 0))
			asm (" swbp 0");

		/* Check if MSMC address is 64 byte aligned */
		if ((((int)ptr & 0x0C000000) == 0x0C000000) && (((int)ptr % 0x40) != 0))
			asm (" swbp 0");
	}
#endif /* CACHE_ALIGNMENT_CHECK */

	/* Disable Interrupts */
	key = Hwi_disable();

	/*  Cleanup the prefetch buffer also. */
	CSL_XMC_invalidatePrefetchBuffer();

	/* Invalidate the cache. */
	CACHE_invL1d  (ptr, size, CACHE_FENCE_WAIT);

	asm	(" nop	4");
	asm	(" nop	4");
	asm	(" nop	4");
	asm	(" nop	4");

	/* Reenable Interrupts. */
	Hwi_restore(key);
}

/**
 *  @b Description
 *  @n
 *      The function is used to indicate that the block of memory has
 *      finished being accessed. If the memory block is cached then the
 *      application would need to ensure that the contents of the cache
 *      are updated immediately to the actual memory.
 *
 *  @param[in]  ptr
 *       Address of memory block
 *  @param[in]  size
 *       Size of memory block
 *  @param[in]  checkAlign
 *       Flags that enables checks for address cache line alignment
 *
 *  @retval
 *      Not Applicable
 */
static void Osal_endMemAccess (void *ptr, uint32_t size, uint32_t checkAlign)
{
	UInt  key;

#ifdef CACHE_ALIGNMENT_CHECK
    /* Validate the address only if required to do so. */
    if (checkAlign == 1)
    {
		/* Check if DDR3 address is 128 byte aligned */
		if ((((int)ptr & 0x80000000) == 0x80000000) && (((int)ptr % 0x80) != 0))
			asm (" swbp 0");

		/* Check if MSMC address is 64 byte aligned */
		if ((((int)ptr & 0x0C000000) == 0x0C000000) && (((int)ptr % 0x40) != 0))
			asm (" swbp 0");
	}
#endif /* CACHE_ALIGNMENT_CHECK */

	/* Disable Interrupts */
	key = Hwi_disable();

	/* Writeback the contents of the cache. */
	CACHE_wbL1d (ptr, size, CACHE_FENCE_WAIT);

	asm	 (" nop	4");
	asm	 (" nop	4");
	asm	 (" nop	4");
	asm	 (" nop	4");

	/* Reenable Interrupts. */
	Hwi_restore(key);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to indicate that a block of memory is 
 *      about to be accessed. If the memory block is cached then this 
 *      indicates that the application would need to ensure that the 
 *      cache is updated with the data from the actual memory.
 * 
 *  @param[in]  ptr
 *       Address of memory block
 *
 *  @param[in]  size
 *       Size of memory block
 *
 *  @retval
 *      Not Applicable
 */
void Osal_qmssBeginMemAccess (void *ptr, uint32_t size)
{
    Osal_beginMemAccess(ptr, size, 0);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to indicate that the block of memory has 
 *      finished being accessed. If the memory block is cached then the 
 *      application would need to ensure that the contents of the cache 
 *      are updated immediately to the actual memory. 
 *
 *  @param[in]  ptr
 *       Address of memory block
 *
 *  @param[in]  size
 *       Size of memory block

 *  @retval
 *      Not Applicable
 */
void Osal_qmssEndMemAccess (void *ptr, uint32_t size)
{
    Osal_endMemAccess(ptr, size, 0);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to allocate a memory block of the specified size.
 *
 *      Note: If the LLD is used by applications on multiple core, the "cppiHeap"
 *      should be in shared memory
 *
 *  @param[in]  num_bytes
 *      Number of bytes to be allocated.
 *
 *  @retval
 *      Allocated block address
 */
void* Osal_cppiMalloc (uint32_t num_bytes)
{
    uint8_t*    ptr;

    /* Do we have enough space? */
    if ((index + num_bytes) > OSAL_POOL_SIZE)
        return NULL;

    /* Get the pointer to the allocated memory. */
    ptr = &globalMemoryPool[index];

    /* Increment index to account for the number of bytes allocated */
    index = index + num_bytes;
    return ptr;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to free a memory block of the specified size allocated 
 *      using Osal_cppiMalloc() API.
 *
 *  @param[in]  ptr
 *      Pointer to the memory block to be cleaned up.
 *
 *  @param[in]  size
 *      Size of the memory block to be cleaned up.
 *
 *  @retval
 *      Not Applicable
 */
void Osal_cppiFree (void *ptr, uint32_t size)
{
    /* Increment the free counter. */
    cppiFreeCounter++;	
}

/**
 *  @b Description
 *  @n  
 *      The function is used to enter a critical section.
 *      Function protects against 
 *      
 *      access from multiple cores 
 *      and 
 *      access from multiple threads on single core
 *
 *  @retval
 *      Handle used to lock critical section
 */
void* Osal_cppiCsEnter (void)
{
    /* Get the hardware semaphore */
    while ((CSL_semAcquireDirect (CPPI_HW_SEM)) == 0);
    return NULL;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to exit a critical section 
 *      protected using Osal_cppiCsEnter() API.
 *
 *  @param[in]  CsHandle
 *      Handle for unlocking critical section.
 *
 *  @retval
 *      Not Applicable
 */
void Osal_cppiCsExit (void *CsHandle)
{
    /* Release the hardware semaphore */ 
    CSL_semReleaseSemaphore (CPPI_HW_SEM);
    return;
}

/**
 *  @b Description
 *  @n  
 *      The function is the CPPI OSAL Logging API which logs 
 *      the messages on the console.
 *
 *  @param[in]  fmt
 *      Formatted String.
 *
 *  @retval
 *      Not Applicable
 */
void Osal_cppiLog (char *fmt, ... )
{
}

/**
 *  @b Description
 *  @n  
 *      The function is used to indicate that a block of memory is 
 *      about to be accessed. If the memory block is cached then this 
 *      indicates that the application would need to ensure that the 
 *      cache is updated with the data from the actual memory.
 *
 *  @param[in]  ptr
 *       Address of memory block
 *
 *  @param[in]  size
 *       Size of memory block
 *
 *  @retval
 *      Not Applicable
 */
void Osal_cppiBeginMemAccess (void *ptr, uint32_t size)
{
    Osal_beginMemAccess(ptr, size, 0);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to indicate that the block of memory has 
 *      finished being accessed. If the memory block is cached then the 
 *      application would need to ensure that the contents of the cache 
 *      are updated immediately to the actual memory. 
 *
 *  @param[in]  ptr
 *       Address of memory block
 *
 *  @param[in]  size
 *       Size of memory block
 *
 *  @retval
 *      Not Applicable
 */
void Osal_cppiEndMemAccess (void *ptr, uint32_t size)
{
    Osal_endMemAccess(ptr, size, 0);
}

/**
 *  @b Description
 *  @n  
 *      The function is used by the packet library to invalidate the cache.
 *
 *  @param[in]  ptr
 *      Pointer to the buffer to be invalidated
 *  @param[in]  size
 *      Size of the buffer to be invalidated
 *
 *  @retval
 *      None
 */
void Osal_pktLibBeginMemAccess(void* ptr, uint32_t size)
{
    Osal_beginMemAccess(ptr, size, 1);
}

/**
 *  @b Description
 *  @n  
 *      The function is used by the packet library to writeback the cache.
 *
 *  @param[in]  ptr
 *      Pointer to the buffer to be written back
 *  @param[in]  size
 *      Size of the buffer to be written back
 *
 *  @retval
 *      None
 */
void Osal_pktLibEndMemAccess(void* ptr, uint32_t size)
{
    Osal_endMemAccess(ptr, size, 1);
}

/**
 *  @b Description
 *  @n  
 *      The function is used by the packet library to invalidate the cache.
 *
 *  @param[in]  heapHandle
 *      Heap Handle to which the packet belongs. 
 *  @param[in]  ptr
 *      Pointer to the buffer to be invalidated
 *  @param[in]  size
 *      Size of the buffer to be invalidated
 *
 *  @retval
 *      None
 */
void Osal_pktLibBeginPktAccess(Pktlib_HeapHandle heapHandle, Ti_Pkt* ptrPkt, uint32_t size)
{
    /* TODO: We should use the 'heapHandle' and compare it with what we got from the
     * 'create/find' HEAP API & depending upon the comparison take appropriate action. 
     * Just for testing we are always invalidating the cache here. */
    Osal_beginMemAccess(ptrPkt, size, 1);
}

/**
 *  @b Description
 *  @n  
 *      The function is used by the packet library to writeback the packet which hasd been 
 *      accessed & updated.
 *
 *  @param[in]  heapHandle
 *      Heap Handle to which the packet belongs. 
 *  @param[in]  ptr
 *      Pointer to the buffer to be invalidated
 *  @param[in]  size
 *      Size of the buffer to be invalidated
 *
 *  @retval
 *      None
 */
void Osal_pktLibEndPktAccess(Pktlib_HeapHandle heapHandle, Ti_Pkt* ptrPkt, uint32_t size)
{
    /* TODO: We should use the 'heapHandle' and compare it with what we got from the
     * 'create/find' HEAP API & depending upon the comparison take appropriate action. 
     * Just for testing we are always writing back the cache here. */

    /* Writeback the contents of the cache. */
    Osal_endMemAccess(ptrPkt, size, 1);
}

/**
 *  @b Description
 *  @n  
 *      The function is used by the packet library to enter a critical section since
 *      a resource is about to be modified.
 *
 *  @param[in]  heapHandle
 *      Heap Handle 
 *
 *  @retval
 *      Critical Section Handle
 */
void* Osal_pktLibEnterCriticalSection(Pktlib_HeapHandle heapHandle)
{
    /* TODO: We should use the 'heapHandle' and compare it with what we got from the
     * 'create/find' HEAP API & depending upon the comparison take appropriate action. 
     * Implementations here could range from a MULTI-THREAD protection if the packets in 
     * the heap are being accessed across multiple threads or MULTI-CORE if the packets
     * are being accessed across multiple cores. 
     *
     * For testing we are not doing any of this so we are simply setting it to NOOP */
    return NULL;
}

/**
 *  @b Description
 *  @n  
 *      The function is used by the packet library to enter a critical section since
 *      a resource is about to be modified.
 *
 *  @param[in]  heapHandle
 *      Heap Handle 
 *  @param[in]  csHandle
 *      Critical Section Handle
 *
 *  @retval
 *      Not Applicable.    
 */
void  Osal_pktLibExitCriticalSection(Pktlib_HeapHandle heapHandle, void* csHandle)
{
    /* TODO: We should use the 'heapHandle' and compare it with what we got from the
     * 'create/find' HEAP API & depending upon the comparison take appropriate action. 
     * Implementations here could range from a MULTI-THREAD protection if the packets in 
     * the heap are being accessed across multiple threads or MULTI-CORE if the packets
     * are being accessed across multiple cores. 
     *
     * For testing we are not doing any of this so we are simply setting it to NOOP */
    return;        
}


/**
 *  @b Description
 *  @n  
 *      The function is used by the application to invalidate the buffer
 *
 *  @param[in]  ptr
 *      Pointer to the buffer to be invalidated
 *  @param[in]  size
 *      Size of the buffer to be invalidated
 *
 *  @retval
 *      None
 */
void appInvalidateBuffer(void* ptr, uint32_t size)
{
    Osal_beginMemAccess (ptr, size, 1);
}

/**
 *  @b Description
 *  @n
 *      The function is used by the resmgr to invalidate the cache.
 *
 *  @param[in]  ptr
 *      Pointer to the buffer to be invalidated
 *  @param[in]  size
 *      Size of the buffer to be invalidated
 *
 *  @retval
 *      None
 */
void Osal_rmBeginMemAccess(void* ptr, uint32_t size)
{
    Osal_beginMemAccess(ptr, size, 1);
}

/**
 *  @b Description
 *  @n
 *      The function is used by the resmgr to writeback the cache.
 *
 *  @param[in]  ptr
 *      Pointer to the buffer to be written back
 *  @param[in]  size
 *      Size of the buffer to be written back
 *
 *  @retval
 *      None
 */
void Osal_rmEndMemAccess(void* ptr, uint32_t size)
{
	Osal_endMemAccess(ptr, size, 1);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to enter a critical section.
 *
 *  @retval
 *      Handle used to lock critical section
 */
void* Osal_rmCsEnter()
{
    return (void *)Hwi_disable();
}

/**
 *  @b Description
 *  @n  
 *      The function is used to exit a critical section 
 *
 *  @param[in]  csHandle
 *      Handle for unlocking critical section.
 *
 *  @retval
 *      Not Applicable
 */
void Osal_rmCsExit (void* csHandle)
{
    Hwi_restore((unsigned int)csHandle);
}

/**
 *  @b Description
 *  @n
 *      OSAL API called by the named resource module to allocate memory for the
 *      named resource. Since named resources are used across all cores the 
 *      memory allocations are done in shared memory (DDR3)
 *
 *  @param[in]  numBytes
 *      Number of bytes to allocate
 *
 *  @retval
 *      Error       - NULL
 *  @retval
 *      Success     - Pointer to the allocated memory block.      
 */
void* Osal_namedResourceAlloc (uint32_t numBytes)
{
    return Memory_alloc ((xdc_runtime_IHeap_Handle)SharedRegion_getHeap(1), numBytes, 0, NULL);
}

/**
 *  @b Description
 *  @n
 *      OSAL API called by the named resource module to free memory for the
 *      named resource. 
 *
 *  @param[in]  ptrMem
 *      Pointer to the memory to be freed up
 *  @param[in]  numBytes
 *      Number of bytes which were allocated
 *
 *  @retval
 *      Not Applicable.
 */
void Osal_namedResourceFree (void* ptrMem, uint32_t numBytes)
{
    Memory_free ((xdc_runtime_IHeap_Handle)SharedRegion_getHeap(1), ptrMem, numBytes);
}

/**
 *  @b Description
 *  @n
 *      OSAL API called by the named resource module for entering the critical
 *      section while trying to access to the database. Since the database
 *      can be accessed across multiple cores this is a hardware semaphore.
 *
 *  @retval
 *      Not Applicable.
 */
void* Osal_namedResourceCSEnter (void)
{
    /* Get the hardware semaphore for protection against multiple core access */
    while ((CSL_semAcquireDirect (NAMED_RES_HW_SEM)) == 0)
    {
    	Task_sleep(1);
    }
    return NULL;
}

/**
 *  @b Description
 *  @n
 *      OSAL API called by the named resource module for exiting the critical
 *      section after the access to the database is complete.
 *
 *  @param[in]  csHandle
 *      Opaque Critical section handle.
 *
 *  @retval
 *      Not Applicable.
 */
void Osal_namedResourceCSExit (void* csHandle)
{
    /* Release the hardware semaphore */ 
    CSL_semReleaseSemaphore (NAMED_RES_HW_SEM);
}

/**
 *  @b Description
 *  @n
 *      OSAL API called by the named resource module for entering a
 *      multi-threaded critical section . Function protects against
 *      access from multiple threads on a single core.
 *
 *  @retval
 *      Not Applicable.
 */
void *Osal_rmMtCsEnter(void *mtSemObj)
{
    return NULL;
}

/**
 *  @b Description
 *  @n
 *      OSAL API called by the named resource module to exit a multi-threaded
 *      critical section protected using Osal_rmMtCsEnter() API.
 *
 *  @param[in]  mtSemObj
 *      multi-thread semaphore object.
 *  @param[in]  csHandle
 *      Opaque Critical section handle.
 *
 *  @retval
 *      Not Applicable.
 */
void Osal_rmMtCsExit(void *mtSemObj, void *CsHandle)
{

}
