/**
 *   @file  main.c
 *
 *   @brief   
 *      Test Code to test the packet library
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012-2013 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <pktlib_test.h>

/* CPPI/QMSS Include Files. */
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/cppi/cppi_desc.h>
#include <ti/drv/qmss/qmss_drv.h>
#include <ti/drv/qmss/qmss_firmware.h>


/* IPC includes */ 
#include <ti/ipc/GateMP.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/ListMP.h>
#include <ti/ipc/SharedRegion.h>

#define PKTLIB_TEST_BYPASS_RM   1
#ifndef PKTLIB_TEST_BYPASS_RM
/* PDK Resource Manager Include Files. */
#include <ti/drv/rm/rm.h>
#include <ti/drv/rm/rm_services.h>

extern const char rmPolicy[];
extern const char rmGrl[];
Char rmServerName[RM_NAME_MAX_CHARS] = "RM_Server";
Char rmClientName[RM_NAME_MAX_CHARS] = "RM_Client";
Rm_ServiceHandle    *rmServerServiceHandle = NULL;
#endif

/**********************************************************************
 ************************** Global Variables **************************
 **********************************************************************/

/* Memory allocated for the buffers is located in shared memory. */
uint8_t     memoryBuffer[NUM_HOST_DESC][MAX_DATA_SIZE];

/* Memory allocated for the buffers is located in shared memory. */
#pragma DATA_SECTION (sharedMemoryBuffer, ".appSharedMemory");
uint8_t     sharedMemoryBuffer[SHARED_NUM_HOST_DESC][SHARED_MAX_DATA_SIZE];

/* Memory allocated for the descriptors. This is 16 bit aligned. */
#pragma DATA_ALIGN (host_region, 128)
uint8_t     host_region[NUM_HOST_DESC * SIZE_HOST_DESC];

/* QMSS device specific configuration */
extern Qmss_GlobalConfigParams  qmssGblCfgParams;

/* CPPI device specific configuration */
extern Cppi_GlobalConfigParams  cppiGblCfgParams;


/* Gobal Memory Heap created for the test. */
Pktlib_HeapHandle   myHeap;

/**********************************************************************
 ************************* Extern Definitions *************************
 **********************************************************************/

/* External Definition for testing shared heaps. */
extern int32_t test_pktLibrarySharedHeaps(Pktlib_HeapHandle sharedHeapHandle);



/**
 *  @b Description
 *  @n  
 *      System Initialization Code. This is added here only for illustrative
 *      purposes and needs to be invoked once during initialization at 
 *      system startup.
 *
 *  @retval
 *      Success     -   0
 *  @retval
 *      Error       -   <0
 */
static int32_t system_init (void)
{
    int32_t             result;
    Qmss_MemRegInfo     memRegInfo;
    Qmss_InitCfg        qmssInitConfig;

    /* Initialize the QMSS Configuration block. */
    memset (&qmssInitConfig, 0, sizeof (Qmss_InitCfg));
    
    /* Initialize the Host Region. */
    memset ((void *)&host_region, 0, sizeof(host_region));

    /* Set up the linking RAM. Use the internal Linking RAM. 
     * LLD will configure the internal linking RAM address and maximum internal linking RAM size if 
     * a value of zero is specified. Linking RAM1 is not used */
    qmssInitConfig.linkingRAM0Base = 0;
    qmssInitConfig.linkingRAM0Size = 0;
    qmssInitConfig.linkingRAM1Base = 0;
    qmssInitConfig.maxDescNum      = 1024;

#ifdef xdc_target__bigEndian
    /* PDSP Configuration: Big Endian */
    qmssInitConfig.pdspFirmware[0].pdspId   = Qmss_PdspId_PDSP1;
    qmssInitConfig.pdspFirmware[0].firmware = &acc48_be;
    qmssInitConfig.pdspFirmware[0].size     = sizeof (acc48_be);
#else
    /* PDSP Configuration: Little Endian */
    qmssInitConfig.pdspFirmware[0].pdspId   = Qmss_PdspId_PDSP1;
    qmssInitConfig.pdspFirmware[0].firmware = &acc48_le;
    qmssInitConfig.pdspFirmware[0].size     = sizeof (acc48_le);
#endif

#ifndef PKTLIB_TEST_BYPASS_RM
    /* Get the RM Handle and pass it. */
    qmssGblCfgParams.qmRmServiceHandle = rmServerServiceHandle;
#endif

    /* Initialize Queue Manager Sub System */
    result = Qmss_init (&qmssInitConfig, &qmssGblCfgParams);
    if (result != QMSS_SOK)
    {
        printf ("Error initializing Queue Manager SubSystem error code : %d\n", result);
        return -1;
    }

    /* Initialize the memory region configuration. */
    memset ((void *)&memRegInfo, 0, sizeof(Qmss_MemRegInfo));

    /* Memory Region 1 Configuration for single core heaps. */
    memRegInfo.descBase         = (uint32_t *)l2_global_address((uint32_t)host_region);
    memRegInfo.descSize         = SIZE_HOST_DESC;
    memRegInfo.descNum          = NUM_HOST_DESC;
    memRegInfo.manageDescFlag   = Qmss_ManageDesc_MANAGE_DESCRIPTOR;
    memRegInfo.memRegion        = Qmss_MemRegion_MEMORY_REGION1;

    /* Set the start index correctly to account for the shared descriptors. */
    memRegInfo.startIndex       = 0;

    /* Initialize and inset the memory region. */
    result = Qmss_insertMemoryRegion (&memRegInfo); 
    if (result < QMSS_SOK)
    {
        printf ("Error inserting memory region: %d\n", result);
        return -1;
    }
 
    /* Initialize CPPI CPDMA */
    result = Cppi_init (&cppiGblCfgParams);
    if (result != CPPI_SOK)
    {
        printf ("Error initializing Queue Manager SubSystem error code : %d\n", result);
        return -1;
    }

    /* CPPI and Queue Manager are initialized. */
    printf ("Debug: Queue Manager and CPPI are initialized.\n");
    return 0;
}

/**
 *  @b Description
 *  @n  
 *      Entry Point for the test code.
 *
 *  @retval
 *      0   - Success
 *  @retval
 *      <0  - Error
 */

int main (void)
{    
    uint32_t            coreNum;
    Pktlib_HeapCfg      heapCfg;
    int32_t             errCode;
#ifndef PKTLIB_TEST_BYPASS_RM
    Qmss_StartCfg       qmssCfg;
    Cppi_StartCfg       cppiCfg;
    int32_t             result;
    Rm_InitCfg          rmInitCfg;
    Rm_Handle           rmServerHandle = NULL;
    Rm_Handle           rmClientHandle = NULL;
    Rm_ServiceHandle    *rmClientServiceHandle = NULL;

    memset(&rmInitCfg,0,sizeof(Rm_InitCfg));
#endif
    /* Get the core Number. */
    coreNum = CSL_chipReadReg (CSL_CHIP_DNUM);

   /* Is this core the system initialization core? */
    if (coreNum == SYSTEM_INIT_CORE)
    {
        /* Debug Message: */
        System_printf ("****************************************\n");
        System_printf ("****** Packet Library Unit Test ********\n");
        System_printf ("****************************************\n");
#ifndef PKTLIB_TEST_BYPASS_RM

        /* Create the Server instance */
        rmInitCfg.instName = &rmServerName[0];
        rmInitCfg.instType = Rm_instType_SERVER;
        rmInitCfg.instCfg.serverCfg.globalResourceList = (void *)(rmGrl);
        rmInitCfg.instCfg.serverCfg.linuxDtb = NULL;
        rmInitCfg.instCfg.serverCfg.globalPolicy = (void *)rmPolicy;
        rmServerHandle = Rm_init(&rmInitCfg, &result);
        System_printf("Core %d: RM Server instance created. Result = %d\n", coreNum, result);
        if (result != RM_OK)
        {
            System_printf ("Error: Initialization of the RM failed error code: %d\n", result);
            return -1;
        }
        System_printf ("Debug: RM Initialization was successful\n");
            
         rmServerServiceHandle = Rm_serviceOpenHandle(rmServerHandle, &result); 
#endif         
        /* YES. Only the system initialization core is responsible for system initialization
         * and executing all the non shared heap tests. */
        if (system_init() < 0)
            return -1;
        
        /* Synchronize all the cores. */
        Ipc_start();
    
        /* Start the QMSS Driver: For the TMDXEVM6614LXE we start the QMSS with 
         * the resource manager we had instantiated. For the other devices we
         * simply start with no PDK resource manager. */
        /* Display the Version Information for all the PDK Drivers */        
        System_printf ("Debug: %s\n", Qmss_getVersionStr());
        System_printf ("Debug: %s\n", Cppi_getVersionStr());
#ifndef PKTLIB_TEST_BYPASS_RM
        System_printf ("Debug: %s\n", Rm_getVersionStr());
        
        /* Initialize the configurations */
        memset ((void *)&qmssCfg, 0, sizeof(Qmss_StartCfg));

        /* Populate the QMSS configuration. */
        qmssCfg.rmServiceHandle = rmServerServiceHandle;

        /* Start the QMSS with the specified configuration. */
        if (Qmss_startCfg(&qmssCfg) != QMSS_SOK)
            return -1;

        memset ((void *)&cppiCfg, 0, sizeof(Cppi_StartCfg));

        /* Populate the CPPI configuration. */
        cppiCfg.rmServiceHandle = rmServerServiceHandle;

        /* Start the CPPI with the resource manager. */
        Cppi_startCfg(&cppiCfg);
#else
        if (Qmss_start() != QMSS_SOK)
            return -1;

#endif
        /* Initialize the Shared Heaps. */
        Pktlib_sharedHeapInit();

        /* Initialize the heap configuration */
        memset((void *)&heapCfg, 0 , sizeof(Pktlib_HeapCfg));

        /* Populate the heap configuration */
        heapCfg.name                    = "My Test Heap";
        heapCfg.memRegion               = Qmss_MemRegion_MEMORY_REGION1;
        heapCfg.sharedHeap              = 0;
        heapCfg.useStarvationQueue      = 0;
        heapCfg.dataBufferSize          = MAX_DATA_SIZE;
        heapCfg.numPkts                 = 16;
        heapCfg.numZeroBufferPackets    = 64;
        heapCfg.dataBufferPktThreshold  = 0;
        heapCfg.zeroBufferPktThreshold  = 0;       
        heapCfg.heapInterfaceTable.data_malloc  = myMalloc;
        heapCfg.heapInterfaceTable.data_free    = myFree;

        /* Create the Local Heap with specified configuration. */
        myHeap = Pktlib_createHeap(&heapCfg, &errCode);
        if (myHeap == NULL)
        {
            System_printf ("Error: Unable to create the heap error code %d\n", errCode);
            return -1;
        }

        /* Test the packet Library API */
        if (test_pktLibrary() < 0)
        {
            System_printf ("Error: PACKET Library Unit Testing FAILED\n"); 
            return -1;
        }

        /* Benchmark the Packet Library with a use-case. */
        if (benchmark_pktLibrary() < 0)
        {
            System_printf ("Error: BENCHMARKING Packet Library FAILED\n"); 
            return -1;
        }

        {
            Pktlib_HeapHandle   sharedHeapHandle;
            Pktlib_HeapStats    startStats;
            Pktlib_HeapStats    endStats;

            /* Initialize the heap configuration */
            memset((void *)&heapCfg, 0 , sizeof(Pktlib_HeapCfg));

            /* Populate the heap configuration */
            heapCfg.name                    = "MySharedHeap";
            heapCfg.memRegion               = Qmss_MemRegion_MEMORY_REGION1;
            heapCfg.sharedHeap              = 1;
            heapCfg.useStarvationQueue      = 0;
            heapCfg.dataBufferSize          = SHARED_MAX_DATA_SIZE;
            heapCfg.numPkts                 = 16;
            heapCfg.numZeroBufferPackets    = 16;
            heapCfg.dataBufferPktThreshold  = 0;
            heapCfg.zeroBufferPktThreshold  = 0;            
            heapCfg.heapInterfaceTable.data_malloc = mySharedMemoryMalloc;
            heapCfg.heapInterfaceTable.data_free   = mySharedMemoryFree;

            /* Create Shared Heap with specified configuration. */
            sharedHeapHandle = Pktlib_createHeap(&heapCfg, &errCode);
            if (sharedHeapHandle == NULL)
            {
                System_printf ("Error: Unable to create the shared heap error code %d\n", errCode);
                return -1;
            }

            /* Get the heap statistics: Before we run the tests. */
            Pktlib_getHeapStats(sharedHeapHandle, &startStats);

#ifndef DEVICE_K2E
            /* Execute the Shared Heap Tests */
            if (test_pktLibrarySharedHeaps(sharedHeapHandle) < 0)
            {
                System_printf ("Error: SHARED HEAP Packet Library FAILED\n");
                return -1;
            }

            /* Get the heap statistics: At the end of the test */
            Pktlib_getHeapStats(sharedHeapHandle, &endStats);

            /* Check for memory leaks: */
            if ((startStats.numPacketsinGarbage  != endStats.numPacketsinGarbage)  || 
                (startStats.numFreeDataPackets   != endStats.numFreeDataPackets)   || 
                (startStats.numZeroBufferPackets != endStats.numZeroBufferPackets))
            return -1;
#endif
        }

        /* Debug Message: */
   	    System_printf ("Debug: All tests passed\n");

        /* Test passed. */
        return 0;
    }
    else
    {
        /* Synchronize all the cores. */
        Ipc_start();
        
#ifndef PKTLIB_TEST_BYPASS_RM  
        /* Start the DSP/ARM Resource Manager for each core: */
         /* Create the Server instance */
        rmInitCfg.instName = &rmClientName[0];
        rmInitCfg.instType = Rm_instType_CLIENT;
        rmInitCfg.instCfg.clientCfg.staticPolicy = (void *)rmPolicy;
        rmClientHandle = Rm_init(&rmInitCfg, &result);
        System_printf("Core %d: RM Server instance created. Result = %d\n", coreNum, result);
        if (result != RM_OK)
        {
            System_printf ("Error: Initialization of the RM failed error code: %d\n", result);
            return -1;
        }
        System_printf ("Debug: RM Initialization was successful\n");
            
        rmClientServiceHandle = Rm_serviceOpenHandle(rmClientHandle, &result);

        /* On each core we need to start the QMSS before we can proceed with using it. */

        /* Display the Version Information for all the PDK Drivers */
        System_printf ("Debug: %s\n", Rm_getVersionStr());
        System_printf ("Debug: %s\n", Qmss_getVersionStr());
        System_printf ("Debug: %s\n", Cppi_getVersionStr());


        /* Initialize the configurations */
        memset ((void *)&qmssCfg, 0, sizeof(Qmss_StartCfg));

        /* Populate the QMSS configuration. */
        qmssCfg.rmServiceHandle = rmClientServiceHandle;

        /* Start the QMSS with the specified configuration. */
        if (Qmss_startCfg(&qmssCfg) != QMSS_SOK)
            return -1;


        /* Initialize the CPPI Configuration. */
        memset ((void *)&cppiCfg, 0, sizeof(Cppi_StartCfg));

        /* Populate the CPPI configuration. */
        cppiCfg.rmServiceHandle = rmClientServiceHandle;

        /* Start the CPPI with the resource manager. */
        Cppi_startCfg(&cppiCfg);
#else
        if (Qmss_start() != QMSS_SOK)
            return -1;
#endif
        /* Test the Packet Library Shared Heaps. */
        if (test_pktLibrarySharedHeaps(NULL) < 0)
            return -1;

        /* Debug Message: */
   	    System_printf ("Debug: Shared Heap Test Passed.\n");
    }
}
