SECTIONS
{
    // Place all the Multicore Shared Libraries at Well Known Fixed Addresses.
    GROUP(MC_SHARED_DATASTRUCTURES)
    {
        .cfgMemorySection:      align=128  // Resource Manager Config Section
    	.cppi:                  align=128  // CPPI LLD Multicore Datastructures
    	.qmss:                  align=128  // QMSS LLD Multicore Datastructures
    	.pktLibSharedMemory:    align=128  // Packet Library Multicore Datastructures
        .appSharedMemory:       align=128  // Application shared memory
	} load=MSMCSRAM

	// Place the Local Packet Library Heaps into L2 memory
    GROUP(PKTLIB_LOCAL_HEAPS)
    {
		.pktLibLocalMemory:
	} load=L2SRAM

    // Place all the RM configuration data. 
    GROUP(MC_RM_CFG_DATA)
    {
        .rm:                    align=128  // RM
	} load=MSMCSRAM
}
