/**
 *   @file  pktlib_osal.h
 *
 *   @brief   
 *      This is the sample OS Adaptation layer which is used by the packet
 *      library module. The OSAL layer can be ported in either of the following 
 *      manners to a native OS:
 *
 *      <b> Approach 1: </b>
 *      @n  Use Prebuilt Libraries
 *           - Ensure that the provide an implementation of all 
 *             Osal_XXX API for their native OS.
 *           - Link the prebuilt libraries with their application.
 *           - Refer to the "example" directory for an example of this
 *       @n <b> Pros: </b>
 *           - Customers can reuse prebuilt TI provided libraries
 *       @n <b> Cons: </b>
 *           - Level of indirection in the API to get to the actual OS call
 *              
 *      <b> Approach 2: </b>
 *      @n  Rebuilt Library 
 *           - Create a copy of this file and modify it to directly 
 *             inline the native OS calls
 *           - Rebuild the Packet library; ensure that the Include 
 *             path points to the directory where the copy of this file 
 *             has been provided.
 *           - Please refer to the "test" directory for an example of this 
 *       @n <b> Pros: </b>
 *           - Optimizations can be done to remove the level of indirection
 *       @n <b> Cons: </b>
 *           - Packet Libraries need to be rebuilt by the customer.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

 
 
 
 #ifndef __PKTLIB_OSAL_H__
#define __PKTLIB_OSAL_H__

#include <ti/runtime/pktlib/pktlib.h>

/** @addtogroup PKTLIB_OSAL_API
 @{ */

extern void  Osal_pktLibBeginMemAccess(void* ptr, uint32_t size);
extern void  Osal_pktLibEndMemAccess(void* ptr, uint32_t size);
extern void* Osal_pktLibEnterCriticalSection(Pktlib_HeapHandle heapHandle);
extern void  Osal_pktLibExitCriticalSection(Pktlib_HeapHandle heapHandle, void* csHandle);
extern void  Osal_pktLibBeginPktAccess(Pktlib_HeapHandle heapHandle, Ti_Pkt* ptrPkt, uint32_t size);
extern void  Osal_pktLibEndPktAccess(Pktlib_HeapHandle heapHandle, Ti_Pkt* ptrPkt, uint32_t size);

/**
 * @brief   The macro is used by the Packet Library to indicate that the
 * shared memory heaps are about to be accessed. If the memory block is 
 * cached then the implementation should invalidate the cache contents
 * to ensure that the Packet Library reads the actual memory and not the
 * cache.
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
       void  Osal_pktLibBeginMemAccess(void* ptr, uint32_t size)
    @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  ptr  - Pointer to the memory
 *  @n  size - Size of the memory being accessed.
 *
 *  <b> Return Value </b>
 *  @n  None
 */
#define Pktlib_osalBeginMemAccess      Osal_pktLibBeginMemAccess

/**
 * @brief   The macro is used by the Packet Library to indicate that the
 * shared memory heaps have been accessed and updated. If the memory block is 
 * cached then the implementation should writeback the cache contents
 * to ensure that the cache and the memory are in sync with each other.
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
       void  Osal_pktLibEndMemAccess(void* ptr, uint32_t size)
    @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  ptr  - Pointer to the memory 
 *  @n  size - Size of the memory
 *
 *  <b> Return Value </b>
 *  @n  None
 */
#define Pktlib_osalEndMemAccess       Osal_pktLibEndMemAccess

/**
 * @brief   This API will be called by the packet library to start 
 * the critical section before any resource is modified. Depending 
 * upon the heap usage (Multicore vs. Multithread); the critical section 
 * implementation will change.
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
       void* Osal_pktLibEnterCriticalSection(Pktlib_HeapHandle heapHandle)
    @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  heapHandle  - Heap which is being accessed
 *
 *  <b> Return Value </b>
 *  @n  Opaque handle to the critical section object
 */
#define Pktlib_osalEnterCriticalSection      Osal_pktLibEnterCriticalSection

/**
 * @brief   This API will be called by the packet library to end 
 * the critical section after the resource is modified. Depending 
 * upon the heap usage (Multicore vs. Multithread); the critical section 
 * implementation will change.
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
       void  Osal_pktLibExitCriticalSection(Pktlib_HeapHandle heapHandle, void* csHandle)
    @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  heapHandle  - Heap which is being accessed
 *  @n  csHandle    - Critical Section Object Handle.
 *
 *  <b> Return Value </b>
 *  @n  None
 */
#define Pktlib_osalExitCriticalSection      Osal_pktLibExitCriticalSection

/**
 * @brief   The macro is used by the Packet Library to indicate that packet
 * is about to be accessed. If the packet is in cached memory the implementation
 * should invalidate the contents of the packet
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
       void  Osal_pktLibBeginPktAccess(Pktlib_HeapHandle heapHandle, Ti_Pkt* ptrPkt, uint32_t size)
    @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  heapHandle  - Heap to which the packet belongs.
 *  @n  ptrPkt      - Pointer to the packet being accessed.
 *  @n  size        - Size of the packet.
 *
 *  <b> Return Value </b>
 *  @n  None
 */
#define Pktlib_osalBeginPktAccess      Osal_pktLibBeginPktAccess

/**
 * @brief   The macro is used by the Packet Library to indicate that packet
 * access has been accessed & updated . If the packet is in cached memory the 
 * implementation should writeback the contents of the packet
 *
 * <b> Prototype: </b>
 *  The following is the C prototype for the expected OSAL API.
 *
 *  @verbatim
       void  Osal_pktLibEndPktAccess(Pktlib_HeapHandle heapHandle, Ti_Pkt* ptrPkt, uint32_t size)
    @endverbatim
 *
 *  <b> Parameter </b>
 *  @n  heapHandle  - Heap to which the packet belongs.
 *  @n  ptrPkt      - Pointer to the packet being accessed.
 *  @n  size        - Size of the packet.
 *
 *  <b> Return Value </b>
 *  @n  None
 */
#define Pktlib_osalEndPktAccess      Osal_pktLibEndPktAccess

/**
@}
*/

#endif /* __PKTLIB_OSAL_H__ */


