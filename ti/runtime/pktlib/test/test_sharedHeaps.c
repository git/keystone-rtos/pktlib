/**
 *   @file  test_sharedHeaps.c
 *
 *   @brief   
 *      Test Code to test the shared heaps in the Packet Library
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <stdint.h>

/* Packet Library Include Files. */
#include <ti/runtime/pktlib/pktlib.h>

/* CSL Chip Functional Layer */
#include <ti/csl/csl_chip.h>
#include <pktlib_test.h>
/**********************************************************************
 *********************** Test Shared Heap Functions *******************
 **********************************************************************/

/**
 *  @b Description
 *  @n  
 *      The function is used to test the shared heaps in the packet library
 *
 *  @retval
 *      Success     -   0
 *  @retval
 *      Error       -   <0
 */
int32_t test_pktLibrarySharedHeaps(Pktlib_HeapHandle sharedHeapHandle)
{
	uint32_t        coreNum;
    Ti_Pkt*         pkt,testPkt;
    Ti_Pkt*         pClonePkt;
    Qmss_QueueHnd   queueHandle;
    uint8_t         isAllocated;
    uint32_t        packetSize = 120;

    /* Get the Core Number. */
    coreNum = CSL_chipReadReg (CSL_CHIP_DNUM);

    /* Debug Message: */
    printf ("------------------------------------------------------\n");

    /* Open a well-defined application queue (in this case APP_QUEUE2) */
    queueHandle = Qmss_queueOpen(Qmss_QueueType_GENERAL_PURPOSE_QUEUE, APP_QUEUE2, &isAllocated);
    if (queueHandle == NULL)
        return NULL;

    /* On all other cores besides the System Initialization core; the Shared Heap Handle
     * passed is NULL and so we need to find the heap using the name. */
    if (sharedHeapHandle == NULL)
    {
        /* This is not the system initialization core. */
        printf ("Debug (Core %d): Waiting for the Shared Heap to be created.\n", coreNum);

        /* Loop around and wait for the Heap to be created by the System Initialization 
         * core before we proceed. */
        sharedHeapHandle = Pktlib_findHeapByName("MySharedHeap");
        while (sharedHeapHandle == NULL)
            sharedHeapHandle = Pktlib_findHeapByName("MySharedHeap");

        /* Detected Shared Heap has been created; now we allocate a packet from this heap */
        pkt = Pktlib_allocPacket(sharedHeapHandle, packetSize);
        if (pkt == NULL)
            return -1;

        /* Writeback the contents of the packet. */
        Pktlib_writebackPkt(pkt);

        /* Debug Message: */
        printf ("Debug (Core %d): Allocated Packet 0x%p: %d bytes & sent to Application Queue\n", 
                coreNum, pkt, Pktlib_getPacketLen(pkt));

        /* Clone the packet: Allocate a zero buffer packet for this purpose. */
        pClonePkt = Pktlib_allocPacket(sharedHeapHandle, 0);
        if (pClonePkt == NULL)
            return -1;

        /* Clone the packets. */
        if (Pktlib_clonePacket(pkt, pClonePkt) < 0)
            return -1;

        /* Writeback the contents of both the packets: Since the clone will increment 
         * the reference counter of the original packet also. */
        Pktlib_writebackPkt(pkt);
        Pktlib_writebackPkt(pClonePkt);

        /* Debug Message: */
        printf ("Debug (Core %d): Cloned Packet 0x%p -> 0x%p\n", coreNum, pkt, pClonePkt);

        /* Push both packets into the application queue. */
        Qmss_queuePushDesc(queueHandle, pkt);        
        Qmss_queuePushDesc(queueHandle, pClonePkt);
    }
    else
    {
        testPkt = Pktlib_allocPacket(sharedHeapHandle, packetSize);
        if (testPkt == NULL)
            return -1;
        
        /* Cleanup the packet we are done. */
        Pktlib_freePacket(testPkt);
            
        /* This is the system initialization core: */
        printf ("Debug (Core %d): Waiting for the packet to arrive.\n", coreNum);

        /* We loop around here till a packet is available */
        pkt = Qmss_queuePop(queueHandle);
        while (pkt == NULL)
            pkt = Qmss_queuePop(queueHandle);

        /* Invalidate the packet contents. */
        Pktlib_invalidatePkt(pkt);

        /* Debug Message: */
        printf ("Debug (Core %d): Detected Packet 0x%p: %d bytes from application queue\n",
                coreNum, pkt, Pktlib_getPacketLen(pkt));        

        /* Ensure that the packet length is correct. */
        if (packetSize != Pktlib_getPacketLen(pkt))
            return -1;

        /* Cleanup the packet we are done. */
        Pktlib_freePacket(pkt);

        /* We loop around here till the cloned packet is available */
        pkt = Qmss_queuePop(queueHandle);
        while (pkt == NULL)
            pkt = Qmss_queuePop(queueHandle);

        /* Invalidate the packet contents. */
        Pktlib_invalidatePkt(pkt);

        /* Debug Message: */
        printf ("Debug (Core %d): Detected Cloned Packet 0x%p: %d bytes from application queue\n",
                coreNum, pkt, Pktlib_getPacketLen(pkt));

        /* Ensure that the packet length is correct. */
        if (packetSize != Pktlib_getPacketLen(pkt))
        {
            printf ("Error (Core %d): Invalid Packet Size %d bytes in Cloned Packet\n", 
                    coreNum, Pktlib_getPacketLen(pkt));
            return -1;
        }

        /* Cleanup the packet. */
        Pktlib_freePacket(pkt);
    }

    /* Close the application queue. */
    Qmss_queueClose(queueHandle);

    /* Shared Heap Test has completed. */
    return 0;
}
