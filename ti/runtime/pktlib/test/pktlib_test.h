/**
 *   @file  pktlibTest.h
 *
 *   @brief   
 *      Test Code to test the packet library
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __PKTLIB_TEST_H__
#define __PKTLIB_TEST_H__
#include <stdio.h>
#include <stdint.h>

/* BIOS/XDC Include Files. */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Memory.h> 
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include <ti/sysbios/family/c64p/Hwi.h>

/* Packet Library Include Files. */
#include <ti/runtime/pktlib/pktlib.h>

/* CSL Include Files */
#include <ti/csl/csl_chip.h>
#include <ti/csl/csl_tsc.h>
#include <ti/csl/csl_cacheAux.h>



/* This is the Number of host descriptors which are available & configured
 * in the memory region for this test. */
#define NUM_HOST_DESC               256

/* This is the size of each descriptor. */
#define SIZE_HOST_DESC              64

/* Maximum Data Size of each buffer */
#define MAX_DATA_SIZE               1024

/* Core responsible for system initialization. */
#define SYSTEM_INIT_CORE            0

/* Number of descriptors which are available in the shared memory */
#define SHARED_NUM_HOST_DESC        32

/* This is the size of each descriptor in the shared memory */
#define SHARED_SIZE_HOST_DESC       64

/* Maximum Data Size of each buffer in the shared memory */
#define SHARED_MAX_DATA_SIZE        256

/* Hardcoded Queue being used within PKTLIB */
#define APP_QUEUE1                  910
#define APP_QUEUE2                  1000

uint8_t* myMalloc(uint32_t size);
void myFree(uint8_t* ptr, uint32_t size);
uint8_t* mySharedMemoryMalloc(uint32_t size);
void mySharedMemoryFree(uint8_t* ptr, uint32_t size);
int32_t benchmark_pktLibrary(void);
int32_t test_pktLibrary(void);
uint32_t l2_global_address (uint32_t addr);
#endif
