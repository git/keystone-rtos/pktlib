/******************************************************************************
 * FILE PURPOSE: Pktlib Test files.
 ******************************************************************************
 * FILE NAME: module.xs
 *
 * DESCRIPTION: 
 *  This file contains the module specification for Pktlib Test
 *  Files
 *
 * Copyright (C) 2009,2013 Texas Instruments, Inc.
 *****************************************************************************/

/* Load the library utility. */
var libUtility = xdc.loadCapsule ("../build/buildlib.xs");

/**************************************************************************
 * FUNCTION NAME : modBuild
 **************************************************************************
 * DESCRIPTION   :
 *  The function is used to add all the source files in the test 
 *  directory into the package.
 **************************************************************************/
function modBuild() 
{
    Pkg.otherFiles[Pkg.otherFiles.length++] = "test/osal.c";
    Pkg.otherFiles[Pkg.otherFiles.length++] = "test/pktlib_osal.h";
    Pkg.otherFiles[Pkg.otherFiles.length++] = "test/pktlib_test.h";
    Pkg.otherFiles[Pkg.otherFiles.length++] = "test/pktlib_test.c";
    Pkg.otherFiles[Pkg.otherFiles.length++] = "test/README.txt";
    Pkg.otherFiles[Pkg.otherFiles.length++] = "test/test_sharedHeaps.c";

    /* Add all the .cmd files to the release package. */
    var testFiles = libUtility.listAllFiles (".txt", "test/k1");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k]; 
        
    var testFiles = libUtility.listAllFiles (".c", "test/k1");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".h", "test/k1");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".cfg", "test/k1");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".cmd", "test/k1");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
          
    /* Add all the .c files to the release package. */
    var testFiles = libUtility.listAllFiles (".c", "test/k2h");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".c", "test/k2k");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];            

    var testFiles = libUtility.listAllFiles (".c", "test/k2e");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".c", "test/k2l");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];

    /* Add all the .h files to the release package. */
    var testFiles = libUtility.listAllFiles (".h", "test/k2h");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".h", "test/k2k");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];

    var testFiles = libUtility.listAllFiles (".h", "test/k2e");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".h", "test/k2l");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];


    /* Add all the .cfg files to the release package. */
    var testFiles = libUtility.listAllFiles (".cfg", "test/k2h");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".cfg", "test/k2k");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];

    var testFiles = libUtility.listAllFiles (".cfg", "test/k2e");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".cfg", "test/k2l");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];

    /* Add all the .cmd files to the release package. */
    var testFiles = libUtility.listAllFiles (".cmd", "test/k2h");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".cmd", "test/k2k");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];            
        
    var testFiles = libUtility.listAllFiles (".cmd", "test/k2e");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".cmd", "test/k2l");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k]; 

    /* Add all the .txt files to the release package. */
    var testFiles = libUtility.listAllFiles (".txt", "test/k2h");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".txt", "test/k2k");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k]; 

    var testFiles = libUtility.listAllFiles (".txt", "test/k2e");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];
        
    var testFiles = libUtility.listAllFiles (".txt", "test/k2l");
    for (var k = 0 ; k < testFiles.length; k++)
        Pkg.otherFiles[Pkg.otherFiles.length++] = testFiles[k];  
}

