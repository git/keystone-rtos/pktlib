/**
 *   @file  pktlib.c
 *
 *   @brief   
 *      The file implements the Packet Buffer Library. The library provides
 *      a well defined API which abstracts the CPPI descriptors and provides
 *      services which can be used by the applications without getting to the
 *      lower level details of the descriptors. 
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**************************************************************************
 *************************** Include Files ********************************
 **************************************************************************/

/* Standard Include Files. */
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

/* Packet Library Include Files */
#include <pktlib_osal.h>
#include <ti/runtime/pktlib/pktlib.h>

/* CSL Cache Module */
#include <ti/csl/cslr.h>

/* CPPI/QMSS Include */
#include <ti/drv/qmss/qmss_drv.h>
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/cppi/cppi_desc.h>

/* For Debug only: */
#ifdef DEBUG_PKTLIB
#include <xdc/runtime/System.h>
#else
#ifndef NULL
#define NULL (0)
#endif
#endif

/* Same for all SOC's. Being pulled out of CSL to avoid
 * device dependency
 */
#define CACHE_L2_LINESIZE   128

/**************************************************************************
 ************************* Local Definitions ******************************
 **************************************************************************/

/** @addtogroup PKT_LIB_SYMBOL
 @{ */

/**
 * @brief   This is the MAXIMUM length of the heap name.
 */
#define PKTLIB_MAX_HEAP_NAME                32

/**
 * @brief   Internal flag which indicates that the packet has been cloned and
 * there are references held on the packet.
 */
#define PKTLIB_CLONE_PACKET                 0x1

/**
 * @brief   Internal flag which indicates that the packet belongs to a shared heap
 */
#define PKTLIB_SHARED_HEAP_PACKET           0x2

/**
 * @brief   Packet Library Heap Index starting bit position.
 */
#define PKTLIB_HEAP_IDX_START_BIT           28   

/**
 * @brief   Packet Library Heap Index ending bit position.
 */
#define PKTLIB_HEAP_IDX_END_BIT             31

/**
 * @brief   Packet Library Ref count starting bit position.
 */
#define PKTLIB_REF_CNT_START_BIT            22   

/**
 * @brief   Packet Library Ref count ending bit position.
 */
#define PKTLIB_REF_CNT_END_BIT              27

/**
 * @brief   Packet Library flags starting bit position.
 */
#define PKTLIB_INT_FLAGS_START_BIT          30

/**
 * @brief   Packet Library flags ending bit position.
 */
#define PKTLIB_INT_FLAGS_END_BIT            31

/**
 * @brief   This is the MAXIMUM numbers of Packet Library heaps which can exist in the system.
 */
#define PKTLIB_MAX_PACKET_HEAP              (1 << (PKTLIB_HEAP_IDX_END_BIT - PKTLIB_HEAP_IDX_START_BIT + 1))

/**
 * @brief   This is the MAX reference count for a packet.
 */
#define PKTLIB_MAX_REF_COUNT                ((1 << (PKTLIB_REF_CNT_END_BIT - PKTLIB_REF_CNT_START_BIT + 1)) - 1)

/**
 * @brief   This is the MAX number of heaps which can be managed by a super heap.
 */
#define PKTLIB_MAX_SUPER_MANAGED_HEAPS      4

/**
@}
*/

/**************************************************************************
 ************************* Local Structures *******************************
 **************************************************************************/

/** @addtogroup PKT_LIB_DATA_STRUCTURE
 @{ */

/** 
 * @brief 
 *  The structure describes the Packet Library information
 *
 * @details
 *  The packet library keeps certain information per packet. This information
 *  is required by the library to provide the required services. This structure
 *  is stored in the descriptor. We currently use the top 10 bits of the orignal
 *  buffer length and the orignal buffer pointer to store this information.
 */
typedef struct Pktlib_Info
{
    /**
     * @brief   Flags which define the type of the packet.
     */
    uint32_t        flags;

    /**
     * @brief  Reserved memory which ensures that the field below maps to the
     * original buffer length. 
     */    
    uint32_t        reserved[4];

    /**
     * @brief   Information which carries the reference count & heap index.
     */
    uint32_t        information;
 
    /**
     * @brief   This is the original packet from where a packet is cloned.
     * This value is applicable only if the packet type above is set to 
     * CLONE
     */
    Ti_Pkt*         donor;
}Pktlib_Info;

#define PKTLIB_BLOCK_STARVATION_COUNT   4
/** 
 * @brief 
 *  The structure describes the Packet Library Heaps
 *
 * @details
 *  The packet library heap has a list of packets with buffers
 *  and without buffers. 
 */
typedef struct Pktlib_Heap
{
    /**
     * @brief   Name of the heap; used to identify it.
     */
    char                name[PKTLIB_MAX_HEAP_NAME];

    /**
     * @brief   Flag which identifies if the heap is a SUPER heap or not?
     */
    uint16_t            isSuperHeap;

    /**
     * @brief   Flag which indicates if starvation queues should be used or not?
     */
    uint16_t            useStarvationQueue;

    /**
     * @brief   This the base starvation queue which has been allocated to the heap.
     */
    Qmss_QueueHnd       baseStarvationQueue;

    /**
     * @brief   This is valid only for super heaps and indicates all the member
     * heaps which it is monitoring. This field is NOT applicable for normal
     * heaps.
     */
    Pktlib_HeapHandle   memberHeapHandles[PKTLIB_MAX_SUPER_MANAGED_HEAPS];

    /**
     * @brief   The threshold are passed during heap creation and the module
     * uses this to setup the queue thresholds in the QMSS queues. If the value
     * in the heap data buffer free queues falls below the threshold this is 
     * recorded by the Navigator infrastructure.
     */
    uint32_t            dataBufferPktThreshold;

    /**
     * @brief   The threshold are passed during heap creation and the module
     * uses this to setup the queue thresholds in the QMSS queues. If the value
     * in the heap zero buffer free queues falls below the threshold this is 
     * recorded by the Navigator infrastructure.
     */
    uint32_t            zeroBufferPktThreshold;    

    /**
     * @brief   Each heap only has packets of a specific data size.
     */
    uint32_t            dataBufferSize;

    /**
     * @brief   Garbage Queue Information
     */
    Qmss_Queue          garbageQueueInfo;

    /**
     * @brief   Garbage Queue Handle 
     */
    Qmss_QueueHnd       garbageQueueHnd;    

    /**
     * @brief   Free queue information which has the queue manager information
     * where all the available packets with buffers are stored. 
     */
    Qmss_Queue          freeQueueInfo;

    /**
     * @brief   Free queue handle which stores all the packets with buffers
     */
    Qmss_QueueHnd       freeQueueHnd;

    /**
     * @brief   Free queue information which has the queue manager information
     * where all the available packets with no buffers are stored. 
     */
    Qmss_Queue          freeZeroQueueInfo;

    /**
     * @brief   Free queue handle which stores all the packets with no buffers.
     */
    Qmss_QueueHnd       freeZeroQueueHnd;

    /**
     * @brief   Descriptor Size of each descriptor.
     */    
    int32_t             descSize;

    /**
     * @brief   Heap Function Table which was passed during configuration.
     */
    Pktlib_HeapIfTable  heapFxnTable;

    /**
     * @brief   Memory region associated with the heap.
     */
    Qmss_MemRegion      memRegion;
    
    /**
     * @brief   These are the number of packets which are associated with the
     * data buffer size 
     */
    uint32_t            numDataBufferPackets;

    /**
     * @brief   These are the number of zero buffer packets.
     */
    uint32_t            numZeroBufferPackets;
}Pktlib_Heap;

/**************************************************************************
 ************************* Global Variables *******************************
 **************************************************************************/

/**
 * @brief   Global list of all the private heaps which are core specific
 */
#ifdef _TMS320C6X
#pragma DATA_SECTION (gPktHeaps, ".pktLibLocalMemory");
#pragma DATA_SECTION (gSuperPktHeaps, ".pktLibLocalMemory");
#pragma DATA_ALIGN   (gSharedPktHeaps, CACHE_L2_LINESIZE)
#pragma DATA_SECTION (gSharedPktHeaps, ".pktLibSharedMemory");
#endif
Pktlib_Heap    gPktHeaps[PKTLIB_MAX_PACKET_HEAP];

/**
 * @brief   Global list of all the super heaps which are core specific
 */
Pktlib_Heap    gSuperPktHeaps[PKTLIB_MAX_PACKET_HEAP];

/**
 * @brief Global list of all the shared heaps which can be present in the system. 
 */
Pktlib_Heap           gSharedPktHeaps[PKTLIB_MAX_PACKET_HEAP];

/**
@}
*/

/**************************************************************************
 ************************ Extern Definitions ******************************
 **************************************************************************/

/**********************************************************************
 ********************* Packet Library Functions ***********************
 **********************************************************************/

/** @addtogroup PKT_LIB_FUNCTION
 @{ */

/**
 *  @b Description
 *  @n  
 *      Internal Utility function is used to get the pointer to the 
 *      packet library information. 
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *
 *  @retval
 *      Pointer to the library information.
 */
static inline Pktlib_Info* Pktlib_getPktLibInfo(Ti_Pkt* pPkt)
{
    return (Pktlib_Info*)((uint8_t*)pPkt + offsetof (Cppi_HostDesc, tagInfo));
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function is used to get the heap index 
 *
 *  @param[in]  ptrPktLibInfo
 *      Pointer to the internal packet library information
 *
 *  @retval
 *      Heap Index.
 */
static inline uint8_t Pktlib_getPktLibHeapIndex(Pktlib_Info* ptrPktLibInfo)
{
    return CSL_FEXTR(ptrPktLibInfo->information, PKTLIB_HEAP_IDX_END_BIT, PKTLIB_HEAP_IDX_START_BIT);
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function is used to set the heap index 
 *
 *  @param[in]  ptrPktLibInfo
 *      Pointer to the internal packet library information
 *  @param[in]  heapIndex
 *      Heap Index to be configured.
 *
 *  @retval
 *      Not Applicable
 */
static inline void Pktlib_setPktLibHeapIndex(Pktlib_Info* ptrPktLibInfo, uint8_t heapIndex)
{
    CSL_FINSR(ptrPktLibInfo->information, PKTLIB_HEAP_IDX_END_BIT, PKTLIB_HEAP_IDX_START_BIT, heapIndex);
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function is used to retreive the "internal" flags
 *
 *  @param[in]  ptrPktLibInfo
 *      Pointer to the internal packet library information
 *
 *  @retval
 *      Packet Library flags
 */
static inline uint8_t Pktlib_getPktLibFlags(Pktlib_Info* ptrPktLibInfo)
{
    return CSL_FEXTR(ptrPktLibInfo->flags, PKTLIB_INT_FLAGS_END_BIT, PKTLIB_INT_FLAGS_START_BIT);
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function is used to set the "internal" flags.
 *
 *  @param[in]  ptrPktLibInfo
 *      Pointer to the internal packet library information
 *  @param[in]  flags
 *      Internal flags to be configured.
 *
 *  @retval
 *      Not Applicable.
 */
static inline void Pktlib_setPktLibFlags(Pktlib_Info* ptrPktLibInfo, uint8_t flags)
{
    CSL_FINSR(ptrPktLibInfo->flags, PKTLIB_INT_FLAGS_END_BIT, PKTLIB_INT_FLAGS_START_BIT, flags);
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function is used to get the reference counter
 *
 *  @param[in]  ptrPktLibInfo
 *      Pointer to the internal packet library information.
 *
 *  @retval
 *      Reference Counter.
 */
static inline uint8_t Pktlib_getCount(Pktlib_Info* ptrPktLibInfo)
{
    return CSL_FEXTR(ptrPktLibInfo->information, PKTLIB_REF_CNT_END_BIT, PKTLIB_REF_CNT_START_BIT);
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function is used to set the reference counter
 *
 *  @param[in]  ptrPktLibInfo
 *      Pointer to the internal packet library information
 *  @param[in]  refCount
 *      Reference counter to be set
 *
 *  @retval
 *      Not Applicable.
 */
static inline void Pktlib_setRefCount(Pktlib_Info* ptrPktLibInfo, uint8_t refCount)
{
    CSL_FINSR(ptrPktLibInfo->information, PKTLIB_REF_CNT_END_BIT, PKTLIB_REF_CNT_START_BIT, refCount);
}

/**
 *  @b Description
 *  @n  
 *      Function returns the heap handle 
 *      to which the packet belongs to.
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *
 *  @retval
 *      Heap handle.
 */
Pktlib_HeapHandle Pktlib_getPktHeap(Ti_Pkt* pPkt)
{
    Pktlib_Info* ptrPktLibInfo;
    uint8_t      heapIndex;

    /* Get the packet library information. */
    ptrPktLibInfo = Pktlib_getPktLibInfo(pPkt);

    /* Get the heap index. */
    heapIndex = Pktlib_getPktLibHeapIndex(ptrPktLibInfo);
    
    if (Pktlib_getPktLibFlags(ptrPktLibInfo) & PKTLIB_SHARED_HEAP_PACKET)
        return &gSharedPktHeaps[heapIndex];
    else
        return &gPktHeaps[heapIndex];
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function which is used to increment the reference
 *      counter for the specified packet.
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *
 *  @retval
 *      Not Applicable.
 */
static inline void Pktlib_incRefCount(Ti_Pkt* pPkt)
{
    Pktlib_Info* ptrPktLibInfo;
    uint8_t      refCount;

    /* Get the packet library information. */
    ptrPktLibInfo = Pktlib_getPktLibInfo(pPkt);

    /* Modify the reference counter */
    refCount = Pktlib_getCount(ptrPktLibInfo) + 1;
    Pktlib_setRefCount(ptrPktLibInfo, refCount);
    return;
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function which is used to decrement the reference
 *      counter for the specified packet.
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *
 *  @retval
 *      Not Applicable.
 */
static inline void Pktlib_decRefCount(Ti_Pkt* pPkt)
{
    Pktlib_Info* ptrPktLibInfo;
    uint8_t      refCount;

    /* Get the packet library information. */
    ptrPktLibInfo = Pktlib_getPktLibInfo(pPkt);

    /* Modify the reference counter */
    refCount = Pktlib_getCount(ptrPktLibInfo) - 1;
    Pktlib_setRefCount(ptrPktLibInfo, refCount);
    return;
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function which is used to get the current reference
 *      counter for the specified packet.
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *
 *  @retval
 *      Current Reference Counter
 */
static inline uint8_t Pktlib_getRefCount(Ti_Pkt* pPkt)
{
    Pktlib_Info* ptrPktLibInfo;

    /* Get the packet library information. */
    ptrPktLibInfo = Pktlib_getPktLibInfo(pPkt);

    return Pktlib_getCount(ptrPktLibInfo);
}
/**
 *  @b Description
 *  @n  
 *      Internal Utility function which is used to check if the
 *      packet is a data buffer packet or a bufferless packet?
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *
 *  @retval
 *      1   -   Data Buffer attached
 *  @retval
 *      0   -   Bufferless packet
 */
static inline uint8_t Pktlib_isDataBufferPkt(Ti_Pkt* pPkt)
{
    Cppi_HostDesc* ptrHostDesc = Pktlib_getDescFromPacket(pPkt);

    /* Check if the original buffer address is valid or not? */
    if (ptrHostDesc->origBuffPtr == 0)
        return 0;

    /* Data buffer was attached. */
    return 1;
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function which modifies the return queue information in the
 *      descriptor to the specified queue. This is just a replacement function and will
 *      be replaced once we get an optimal CPPI API 
 *
 *  @param[in]  descType
 *      Type of descriptor - Cppi_DescType_HOST, Cppi_DescType_MONOLITHIC
 *  @param[in]  descAddr
 *      Memory address of descriptor.
 *  @param[in]  queue
 *      Queue Manager - 0 or 1.
 *      Queue Number - 0 to 4094 with in queue manager 0 or 1.
 *
 *  @retval
 *      Not applicable
 */
static inline void Pktlib_cppiSetReturnQueue(Cppi_DescType descType, Cppi_Desc* descAddr, Qmss_Queue* queue)
{
    Cppi_HostDesc* ptrHostDesc = (Cppi_HostDesc *)descAddr;

    /* Clear out the lower order 14 bits of the packet information */
    ptrHostDesc->packetInfo = ptrHostDesc->packetInfo & 0xFFFFC000;

    /* Set the Garbage Queue Information in the descriptor. */
    ptrHostDesc->packetInfo = CSL_FMKR(13, 12, queue->qMgr) |
                              CSL_FMKR(11, 0,  queue->qNum) |
                              ptrHostDesc->packetInfo;
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function which modifies the return queue information in the
 *      descriptor to the garbage queue.
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *
 *  @retval
 *      Not applicable
 */
static inline void Pktlib_setReturnQueueToGarbage(Ti_Pkt* pPkt)
{
    Pktlib_Heap*   ptrPktHeap;

    /* Get the heap information. */
    ptrPktHeap = (Pktlib_Heap*)Pktlib_getPktHeap(pPkt);

    /* Set the return queue information to be the Heap Garbage Queue in the descriptor. */
    Pktlib_cppiSetReturnQueue(Cppi_DescType_HOST, (Cppi_Desc*)pPkt, &ptrPktHeap->garbageQueueInfo);
}

/**
 *  @b Description
 *  @n  
 *      This function is used to read the starvation count. We cannot use the QMSS LLD API
 *      for this purpose because the register is a Clear-on-read.
 *
 *  @param[in]  ptrPktHeap
 *      Pointer to the heap for which the statistics are required.
 *  @param[out]  dataBufferCount
 *      This is the data buffer starvation counter
 *  @param[out]  zeroDataBufferCount
 *      This is the zero data buffer starvation counter
 *
 *  @retval
 *      Success -   0
 *  @retval
 *      Error   -   <0
 */
static void Pktlib_getQueueStarvationCount
(
    Pktlib_Heap* ptrPktHeap,
    uint8_t*     dataBufferCount,
    uint8_t*     zeroDataBufferCount
)
{
    uint32_t    starvationCount[PKTLIB_BLOCK_STARVATION_COUNT];

    *dataBufferCount = 0;
    Qmss_getStarvationCounts(ptrPktHeap->baseStarvationQueue,
                             PKTLIB_BLOCK_STARVATION_COUNT,
                             starvationCount);
    *dataBufferCount = starvationCount[0];
    *zeroDataBufferCount = starvationCount[1];

    return;
}

/**
 *  @b Description
 *  @n  
 *      This function is used to allocate a starvation queue. The starvation counters
 *      in the Navigator infrastructure are a clear on read and also there is no byte
 *      access to this register so a read for a queue would imply that all 4 queues 
 *      in that register bank will have there counters reset. This function thus ensures
 *      that all allocations fit in the same register and the rest of the queues 
 *      are marked as reserved.
 *
 *  @param[out]  baseStarvationQueue
 *      Pointer to the block of starvation queues allocated.
 *
 *  @retval
 *      Success -   0
 *  @retval
 *      Error   -   <0
 */
static int32_t Pktlib_allocateStarvationQueue (Qmss_QueueHnd* pBaseStarvationQueue)
{ 
    Qmss_QueueHnd   starvationQueueBlock[PKTLIB_BLOCK_STARVATION_COUNT];    
    
    *pBaseStarvationQueue = Qmss_queueBlockOpen(starvationQueueBlock,
                                            Qmss_QueueType_STARVATION_COUNTER_QUEUE,
                                            PKTLIB_BLOCK_STARVATION_COUNT,
                                            PKTLIB_BLOCK_STARVATION_COUNT);
    if (*pBaseStarvationQueue < 0)
    {
#ifdef DEBUG_PKTLIB
        /* Debug only: */
        System_printf ("Debug: QMSS Error allocating Starvation Queue Block:%d \n", 
                        *pBaseStarvationQueue);
#endif
         return -1;
    }
    return 0;
}

/**
 *  @b Description
 *  @n  
 *      Internal Utility function which is used to get the descriptor size for 
 *      the specified memory region.
 *  
 *  @param[in]  memRegion
 *      Memory region for which the descriptor size is required.
 *
 *  @retval
 *      Error           - -1
 *  @retval
 *      Success         - Descriptor Size
 */
static int32_t Pktlib_getDescSize (Qmss_MemRegion memRegion)
{
    Qmss_MemRegCfg      memRegionInfo;
    uint16_t            index;

    /* Get all the memory region configuration */
    if (Qmss_getMemoryRegionCfg (&memRegionInfo) != QMSS_SOK)
        return -1;

    /* Cycle through allthe memory regions. */
    for (index = 0; index < QMSS_MAX_MEM_REGIONS; index++)
    {
        /* Did we get a match? If so return the descriptor size */
        if (memRegionInfo.memRegInfo[index].memRegion == memRegion)
            return memRegionInfo.memRegInfo[index].descSize;
    }

    /* No match was found we return error */
    return -1;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to initialize the packet library module shared memory
 *      Heap. This should be called before any shared memory heaps are created or used
 *      and should be done only once in the entire system.
 *
 *  @retval
 *      Error           - -1
 *  @retval
 *      Success         - 0
 */
int32_t Pktlib_sharedHeapInit (void)
{
    uint16_t    heapIndex;

#ifdef DEBUG_PKTLIB
    /* Debug only: */
    System_printf ("Debug: Size of the Packet Heap is %d bytes\n", sizeof(Pktlib_Heap));
#endif

    /* Basic Validation: We need to ensure we are cache line aligned. */
    if ((sizeof(Pktlib_Heap) % CACHE_L2_LINESIZE) != 0)
        return -1;

    /* Initialize the shared memory heaps. */
    for (heapIndex = 0; heapIndex < PKTLIB_MAX_PACKET_HEAP; heapIndex++)
    {
        /* Initialize the shared memory heap. */
        memset ((void *)&gSharedPktHeaps[heapIndex], 0, sizeof(Pktlib_Heap));
  
        /* Writeback the contents of the cache to ensure that all other cores see the initialized memory. */
        Pktlib_osalEndMemAccess(&gSharedPktHeaps[heapIndex], sizeof(Pktlib_Heap));
    }

    /* Packet Library has been initialized successfully. */
    return 0;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to create a heap. There can exist multiple heaps
 *      in the system. Each heap has a specific set of properties and can be
 *      used by applications to have buffers & descriptors residing in different
 *      memory regions with different properties etc.
 *
 *  @param[in]  ptrHeapCfg
 *      Heap Configuration using which the heap is to be created
 *  @param[out]  errCode
 *      Error code populated if there was an error.
 *
 *  @sa
 *      Pktlib Error Codes
 *
 *  @retval
 *      Success -   Heap Handle 
 *  @retval
 *      Error   -   NULL (refer to the error code for more details)
 */
Pktlib_HeapHandle Pktlib_createHeap(Pktlib_HeapCfg* ptrHeapCfg, int32_t* errCode)
{
    uint16_t        heapIndex;
    Pktlib_Info*    ptrPktLibInfo;
    uint16_t        index;
    Pktlib_Heap*    ptrPktHeap;
    Cppi_DescCfg    descCfg;
    Cppi_HostDesc*  ptrHostDesc;
    Qmss_QueueHnd   queueHandle;
    uint8_t*        ptr_dataBuffer;
    uint32_t        numAllocated;
    uint8_t         isAllocated;
    uint8_t         packetFlag;

    /* Initialize the error code */
    *errCode = PKTLIB_EINVAL;

    /* Basic Validations: Ensure that a valid heap configuration was passed */
    if (ptrHeapCfg == NULL)
        return NULL;

    /* Basic Validation: Ensure that the heap has a valid name passed to it */
    if (ptrHeapCfg->name == NULL)
        return NULL;    

    /* Basic Validation: Heaps should either have packets with buffers or packets without buffers. */
    if ((ptrHeapCfg->numPkts == 0) && (ptrHeapCfg->numZeroBufferPackets == 0))
        return NULL;

    /* Basic Validation: If there are buffers to be allocated ensure that a valid alloc and free
     * API Was passed in the interface table */
    if (ptrHeapCfg->numPkts != 0)
    {
        if ((ptrHeapCfg->heapInterfaceTable.data_malloc == NULL) || 
            (ptrHeapCfg->heapInterfaceTable.data_free == NULL))
            return NULL;
    }

    /* Basic Validation: For Shared heaps we always need a cache invalidate & writeback */
    if (ptrHeapCfg->sharedHeap == 1)
    {
        /* Set the packet flags correctly indicating that the packet belongs to the Shared Heap */
        packetFlag = PKTLIB_SHARED_HEAP_PACKET;

        /* Check Shared heaps to get a free entry in the heap table which can be used. */
        for (heapIndex = 0; heapIndex < PKTLIB_MAX_PACKET_HEAP; heapIndex++)
        {
            /* Invalidate the shared memory heap */
            Pktlib_osalBeginMemAccess(&gSharedPktHeaps[heapIndex], sizeof(Pktlib_Heap));

            /* If the name is NULL implies that the heap is not being used. */
            if (gSharedPktHeaps[heapIndex].name[0] == 0x0)
                break;
        }

        /* Did we get a free heap? */
        if (heapIndex == PKTLIB_MAX_PACKET_HEAP)
        {
            *errCode = PKLIB_EMAXHEAPLIMIT;
            return NULL;
        }

        /* Get the pointer to the shared packet heap. */
        ptrPktHeap = &gSharedPktHeaps[heapIndex];
    }
    else
    {
        /* Single Core Heap: Cycle through all the heaps in the system and get a free one. */
        for (heapIndex = 0; heapIndex < PKTLIB_MAX_PACKET_HEAP; heapIndex++)
        {
            /* If the name is NULL implies that the heap is not being used. */
            if (gPktHeaps[heapIndex].name[0] == 0x0)
                break;
        }      

        /* Did we get a free heap? */
        if (heapIndex == PKTLIB_MAX_PACKET_HEAP)
        {
            *errCode = PKLIB_EMAXHEAPLIMIT;
            return NULL;
        }

        /* Get the pointer to the packet heap. */
        ptrPktHeap = &gPktHeaps[heapIndex];

        /* Set the packet flags correctly indicating that the packet belongs to the SINGLECORE Heap */
        packetFlag = 0;
    }

    /* Initialize the heap. */
    memset ((void *)ptrPktHeap, 0, sizeof(Pktlib_Heap));

    /* Copy heap configuration parameters to the heap */
    memcpy ((void *)&ptrPktHeap->heapFxnTable, (void *)&ptrHeapCfg->heapInterfaceTable, sizeof(Pktlib_HeapIfTable));
    ptrPktHeap->memRegion            = ptrHeapCfg->memRegion;
    ptrPktHeap->numDataBufferPackets = ptrHeapCfg->numPkts;
    ptrPktHeap->numZeroBufferPackets = ptrHeapCfg->numZeroBufferPackets;

    /* Get the descriptor size */
    ptrPktHeap->descSize = Pktlib_getDescSize(ptrHeapCfg->memRegion);
    if (ptrPktHeap->descSize < 0)
    {
        /* This can fail if we were passed a memory region which does not exist */
        *errCode = PKTLIB_EINVAL;
        return NULL;
    }

    /* Store the heap configuration parameters */
    ptrPktHeap->useStarvationQueue     = ptrHeapCfg->useStarvationQueue;
    ptrPktHeap->dataBufferPktThreshold = ptrHeapCfg->dataBufferPktThreshold;
    ptrPktHeap->zeroBufferPktThreshold = ptrHeapCfg->zeroBufferPktThreshold;
    ptrPktHeap->dataBufferSize         = ptrHeapCfg->dataBufferSize;

    /* Is this a heap which uses starvation queues? */
    if (ptrPktHeap->useStarvationQueue)
    {
        /* YES. So we need to allocate a starvation queue number which can be used. */
        if (Pktlib_allocateStarvationQueue(&ptrPktHeap->baseStarvationQueue) < 0)
        {
            *errCode = PKLIB_ERESOURCE;
            return NULL;
        }
#ifdef DEBUG_PKTLIB
        System_printf ("Debug: Starvation Queue 0x%x has been allocated for the heap\n", 
                        ptrPktHeap->baseStarvationQueue);
#endif
    }

    /* Are there packets with data buffers? */
    if (ptrHeapCfg->numPkts != 0)
    {
        /* YES. Check if we need to allocate a STARVATION Counter queue or not? */
        if (ptrPktHeap->useStarvationQueue)
        {
            /* Allocate a starvation counter queue. */
            ptrPktHeap->freeQueueHnd = ptrPktHeap->baseStarvationQueue;
            if (ptrPktHeap->freeQueueHnd == 0)
            {
                *errCode = PKLIB_ERESOURCE;
                return NULL;
            }
        }
        else
        {
            /* Allocate a general purpose queue where these packets will be stored. */
            ptrPktHeap->freeQueueHnd = Qmss_queueOpen(Qmss_QueueType_GENERAL_PURPOSE_QUEUE,
                                                      QMSS_PARAM_NOT_SPECIFIED,
                                                      &isAllocated);
            if (ptrPktHeap->freeQueueHnd == 0)
            {
                *errCode = PKLIB_ERESOURCE;
                return NULL;
            }
        }
    }

    /* Are there packets with zero data buffers? */
    if (ptrHeapCfg->numZeroBufferPackets != 0)
    {
        /* YES. Check if we need to allocate a STARVATION Counter queue or not? */
        if (ptrPktHeap->useStarvationQueue)
        {
            /* Allocate a starvation queue where these packets will be stored. */
            ptrPktHeap->freeZeroQueueHnd = Qmss_queueOpen(Qmss_QueueType_STARVATION_COUNTER_QUEUE,
                                                          ptrPktHeap->baseStarvationQueue + 1, 
                                                          &isAllocated);            
            if (ptrPktHeap->freeZeroQueueHnd == 0)
            {
                *errCode = PKLIB_ERESOURCE;
                return NULL;
            }
        }
        else
        {
            /* Allocate a general purpose queue where these packets will be stored. */
            ptrPktHeap->freeZeroQueueHnd = Qmss_queueOpen(Qmss_QueueType_GENERAL_PURPOSE_QUEUE,
                                                          QMSS_PARAM_NOT_SPECIFIED, 
                                                          &isAllocated);
            if (ptrPktHeap->freeZeroQueueHnd == 0)
            {
                *errCode = PKLIB_ERESOURCE;
                return NULL;
            }
        }
    }

    /* Set the queue threshold: Initially we configure the thresholds so that they are set only if the
     * number of packets in the queue exceed the MAX allowed. This is done because the queues are empty 
     * in the beginning and so this will cause a false low detection. 
     *  - To be able to work with the thresholds the MPU needs to be configured. Please refer to the
     *    Navigator UG for more information. If this is NOT done the configuration of thresholds will 
     *    NOT work; so here we also check for this condition. */
    if (ptrHeapCfg->numZeroBufferPackets != 0)
    {
        /* Zero Buffer Packets are configured. Is the zero buffer data threshold setup? */
        if (ptrHeapCfg->zeroBufferPktThreshold > 0)
        {
            /* YES. Set the Queue threshold to the MAX value. */
            Qmss_setQueueThreshold(ptrPktHeap->freeZeroQueueHnd, 1, 10);

            /* Check if the MPU has been properly setup? */
            if (Qmss_getQueueThreshold(ptrPktHeap->freeZeroQueueHnd) != 10)
            {
                *errCode = PKTLIB_EPERM;
                return NULL;
            }
        }
    }

    /* Are data buffer packets present in the heap. */
    if (ptrHeapCfg->numPkts != 0)
    {
        /* Data Buffer packets are configured. Is the data buffer threshold setup? */
        if (ptrHeapCfg->dataBufferPktThreshold > 0)
        {
            /* YES. Set the Queue threshold to the MAX value. */
            Qmss_setQueueThreshold(ptrPktHeap->freeQueueHnd, 1, 10);

            /* Check if the MPU has been properly setup? */
            if (Qmss_getQueueThreshold(ptrPktHeap->freeQueueHnd) != 10)
            {
                *errCode = PKTLIB_EPERM;
                return NULL;
            }
        }
    }

    /* Initialize the descriptor configuration */
    memset ((void *)&descCfg, 0, sizeof(Cppi_DescCfg));

    /* Populate the descriptor configuration:  */
    descCfg.memRegion                 = ptrHeapCfg->memRegion;
	descCfg.descNum                   = ptrHeapCfg->numPkts + ptrHeapCfg->numZeroBufferPackets;
	descCfg.destQueueNum              = QMSS_PARAM_NOT_SPECIFIED;
	descCfg.queueType                 = Qmss_QueueType_GENERAL_PURPOSE_QUEUE;
	descCfg.initDesc                  = Cppi_InitDesc_INIT_DESCRIPTOR;
	descCfg.descType                  = Cppi_DescType_HOST;
	descCfg.returnQueue.qMgr          = QMSS_PARAM_NOT_SPECIFIED;
    descCfg.returnQueue.qNum          = QMSS_PARAM_NOT_SPECIFIED;
	descCfg.epibPresent               = Cppi_EPIB_EPIB_PRESENT;
    descCfg.returnPushPolicy          = Qmss_Location_TAIL;
	descCfg.cfg.host.returnPolicy     = Cppi_ReturnPolicy_RETURN_BUFFER;
	descCfg.cfg.host.psLocation       = Cppi_PSLoc_PS_IN_DESC;

    /* Initialize the descriptors and place them into a temporary queue. */
	queueHandle = Cppi_initDescriptor (&descCfg, &numAllocated);
	if (queueHandle < 0)
    {
        /* Setup the error code properly. */
        *errCode = (int32_t)queueHandle;
	    return NULL;
    }

    /* Make sure we were allocated what we requested for*/
    if (numAllocated != descCfg.descNum)
    {
        /* Setup the error code properly. */
        *errCode = PKLIB_ERESOURCE;
        return NULL;
    }

    /* Create the garbage queue. */
    ptrPktHeap->garbageQueueHnd = Qmss_queueOpen(Qmss_QueueType_GENERAL_PURPOSE_QUEUE,
                                                 QMSS_PARAM_NOT_SPECIFIED, &isAllocated);
    if (ptrPktHeap->garbageQueueHnd == 0)
    {
        /* Setup the error code properly. */
        *errCode = PKLIB_ERESOURCE;
        return NULL;            
    }

    /* Get the queue information for the Garbage queue. */
    ptrPktHeap->garbageQueueInfo = Qmss_getQueueNumber(ptrPktHeap->garbageQueueHnd);

    /* Get the queue information of the Zero Buffer Queue. */
    ptrPktHeap->freeZeroQueueInfo = Qmss_getQueueNumber(ptrPktHeap->freeZeroQueueHnd);

    /* Allocate the specified number of zero packets and place them into the corresponding queue. */
    for (index = 0; index < ptrHeapCfg->numZeroBufferPackets; index++)
    {
        /* Pop a descriptor of the free queue. */
        ptrHostDesc = (Cppi_HostDesc*)QMSS_DESC_PTR(Qmss_queuePop(queueHandle));
        if (ptrHostDesc == NULL)
        {
            /* Setup the error code properly. */
            *errCode = PKLIB_ERESOURCE;
            return NULL;            
        }

        /* Ensure that the packet is marked to belong to the specified heap & also set the flag
         * indicating that this is a ZERO Buffer Packet. */
        ptrPktLibInfo = Pktlib_getPktLibInfo((Ti_Pkt*)ptrHostDesc);

        /* Reset the orignal buffer information in the descriptor. */
        Cppi_setOriginalBufInfo(Cppi_DescType_HOST, (Cppi_Desc*)ptrHostDesc, NULL, 0);

        /* Set the packet information fields in the packet. */
        Pktlib_setPktLibHeapIndex(ptrPktLibInfo, heapIndex);
        Pktlib_setPktLibFlags(ptrPktLibInfo, packetFlag);

        /* All zero buffer packets which will be used only for cloning are by default to be 
         * cleaned up by default if passed to the IP blocks to the garbage queues. This is 
         * because cloned packets will hold references */
        Pktlib_cppiSetReturnQueue(Cppi_DescType_HOST, (Cppi_Desc*)ptrHostDesc, &ptrPktHeap->garbageQueueInfo);

        /* Writeback the contents of the packet back into the memory  
         * We use the other OSAL Writeback command here because we are still creating a heap and
         * the application does not have access to the heap handle. */
        Pktlib_osalEndMemAccess (ptrHostDesc, ptrPktHeap->descSize);

        /* Push this descriptor into the corresponding queue */
        Qmss_queuePushDesc(ptrPktHeap->freeZeroQueueHnd, (Cppi_Desc*)ptrHostDesc);
    }

    /* Get the queue information of the Data Buffer Queue. */
    ptrPktHeap->freeQueueInfo = Qmss_getQueueNumber(ptrPktHeap->freeQueueHnd);

    /* Allocate the specified number of data packets and place them into the corresponding queue. */
    for (index = 0; index < ptrHeapCfg->numPkts; index++)
    {
        /* Pop a descriptor of the free queue. */
        ptrHostDesc = (Cppi_HostDesc*)QMSS_DESC_PTR(Qmss_queuePop(queueHandle));
        if (ptrHostDesc == NULL)
        {
            /* Setup the error code properly. */
            *errCode = PKLIB_ERESOURCE;
            return NULL;
        }

        /* Allocate memory for the data buffer. */
	    ptr_dataBuffer = ptrHeapCfg->heapInterfaceTable.data_malloc(ptrHeapCfg->dataBufferSize);
        if (ptr_dataBuffer == NULL)
        {
            /* Setup the error code properly. */
            *errCode = PKTLIB_ENOMEM;
            return NULL;
        }

        /* Ensure that the packet is marked to belong to the specified heap and set the flag
         * indicating that this is a BUFFER packet. */
        ptrPktLibInfo = Pktlib_getPktLibInfo((Ti_Pkt*)ptrHostDesc);

        /* Set the orignal buffer information in the descriptor. */
        Cppi_setOriginalBufInfo(Cppi_DescType_HOST, (Cppi_Desc*)ptrHostDesc, 
                                ptr_dataBuffer, ptrHeapCfg->dataBufferSize);

        /* Setup the packet flags with the appropriate information. */
        Pktlib_setPktLibHeapIndex(ptrPktLibInfo, heapIndex);
        Pktlib_setPktLibFlags(ptrPktLibInfo, packetFlag);

        /* Set the data and payload length into the packet. */
	    Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc*)ptrHostDesc, (uint8_t*)ptr_dataBuffer, ptrHeapCfg->dataBufferSize);

        /* Set the return queue to be the free queue */
        Pktlib_cppiSetReturnQueue(Cppi_DescType_HOST, (Cppi_Desc*)ptrHostDesc, &ptrPktHeap->freeQueueInfo);

        /* Writeback the contents of the packet back into the memory
         * We use the other OSAL Writeback command here because we are still creating a heap and
         * the application does not have access to the heap handle. */
        Pktlib_osalEndMemAccess (ptrHostDesc, ptrPktHeap->descSize);

        /* Push this descriptor into the corresponding queue */
        Qmss_queuePushDesc(ptrPktHeap->freeQueueHnd, (Cppi_Desc*)ptrHostDesc);
    }

    /* Close the temporary queue */
    Qmss_queueClose(queueHandle);

#if 1
    /* Setup the queue thresholds after data has been placed into the queues. */
    if (ptrHeapCfg->numZeroBufferPackets != 0)
    {
        if (ptrHeapCfg->zeroBufferPktThreshold > 0)
        {
            uint32_t    threshold = 1;
            uint32_t    x = (ptrHeapCfg->zeroBufferPktThreshold + 1);
            while (1)
            {
                x = x / 2;
                if (x <= 1)
                    break;
                threshold++;
            }
#ifdef DEBUG_PKTLIB
            System_printf ("Debug: Zero Threshold is %d computed threshold is %d\n", 
                            ptrHeapCfg->zeroBufferPktThreshold, threshold);
#endif
            /* Setup the Queue threshold */
            Qmss_setQueueThreshold(ptrPktHeap->freeZeroQueueHnd, 0, threshold);
        }        
    }
    if (ptrHeapCfg->numPkts != 0)
    {
        if (ptrHeapCfg->dataBufferPktThreshold > 0)
        {
            uint32_t    threshold = 1;
            uint32_t    x = (ptrHeapCfg->dataBufferPktThreshold + 1);
            while (1)
            {
                x = x / 2;
                if (x <= 1)
                    break;
                threshold++;
            }
#ifdef DEBUG_PKTLIB
            System_printf ("Debug: Data Threshold is %d computed threshold is %d\n", 
                            ptrHeapCfg->dataBufferPktThreshold, threshold);
#endif
            /* Setup the Queue threshold */
            Qmss_setQueueThreshold(ptrPktHeap->freeQueueHnd, 0, threshold);                
        }
    }    
#else
    /* Setup the queue thresholds after data has been placed into the queues. */
    if (ptrHeapCfg->numZeroBufferPackets != 0)
    {
        if (ptrHeapCfg->zeroBufferPktThreshold > 0)
            Qmss_setQueueThreshold(ptrPktHeap->freeZeroQueueHnd, 0, ptrHeapCfg->zeroBufferPktThreshold);
    }
    if (ptrHeapCfg->numPkts != 0)
    {
        if (ptrHeapCfg->dataBufferPktThreshold > 0)
            Qmss_setQueueThreshold(ptrPktHeap->freeQueueHnd, 0, ptrHeapCfg->dataBufferPktThreshold);                
    }    
#endif

    /* The heap has been successfully allocated so initialize all the fields */
    strncpy(ptrPktHeap->name, ptrHeapCfg->name, PKTLIB_MAX_HEAP_NAME-1);

    /* Writeback the cache contents. */
    Pktlib_osalEndMemAccess(ptrPktHeap, sizeof(Pktlib_Heap));

    /* There was no error and the heap has been successfully created. */
    *errCode = 0;
    return ptrPktHeap;
}

/**
 *  @b Description
 *  @n  
 *      Internal function which compares the data buffer sizes of the 2 heaps
 *
 *  @param[in]  a
 *      Handle to the Heap1 to be compared
 *  @param[in]  b
 *      Handle to the Heap1 to be compared
 *
 *  @retval
 *      0   - Data Buffer Sizes are the same
 *  @retval
 *      1   - Data Buffer size of Heap 'a' is greater than Data Buffer size of Heap 'b'
 *  @retval
 *      <0  - Data Buffer size of Heap 'a' is less than Data Buffer size of Heap 'b'
 */
static int32_t Pktlib_cmpDataBufferSize(Pktlib_HeapHandle a, Pktlib_HeapHandle b)
{
	if (Pktlib_getMaxBufferSize(a) > Pktlib_getMaxBufferSize(b))
        return 1;
	else if (Pktlib_getMaxBufferSize(a) < Pktlib_getMaxBufferSize(b))
        return -1;
	else
        return 0;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to sort the heaps on the basis of the data buffer size.
 *
 *  @param[in]  memberHeaps
 *      Array of the heaps which have to be sorted.
 *  @param[in]  len
 *      Number of heaps to be sorted.
 *
 *  @retval
 *      Not Applicable.
 */
static void Pktlib_sortOnDataBufferSize
(
    Pktlib_HeapHandle memberHeaps[],
    uint32_t          len
)
{
    uint32_t                indx;
    uint32_t                heapStartIndex = 0;
    Pktlib_HeapHandle       cur_val;
    Pktlib_HeapHandle       prev_val;

	prev_val = memberHeaps[heapStartIndex];

	for (indx = heapStartIndex+1; indx < len; ++indx)
	{
		cur_val = memberHeaps[indx];
		if (Pktlib_cmpDataBufferSize(prev_val, cur_val) > 0)
		{
			/* out of order: array[indx-1] > array[indx] */
			uint32_t    indx2;
			memberHeaps[indx] = prev_val; /* move up the larger item first */

			/* find the insertion point for the smaller item */
			for (indx2 = indx - 1; indx2 > 0;)
			{
				Pktlib_HeapHandle temp_val = memberHeaps[indx2 - 1];
				if (Pktlib_cmpDataBufferSize(temp_val, cur_val) > 0)
				{
					memberHeaps[indx2--] = temp_val;
					/* still out of order, move up 1 slot to make room */
				}
				else
					break;
			}
			memberHeaps[indx2] = cur_val; /* insert the smaller item right here */
		}
		else
		{
			/* in order, advance to next element */
			prev_val = cur_val;
		}
	}
}

/**
 *  @b Description
 *  @n  
 *      The function is used to create a super heap. The super heap is a collection
 *      of multiple heaps of different data buffer sizes. When an application tries 
 *      to allocate memory using a super heap the 'best' size heap is used to fit 
 *      the allocationrequest. This allows applications to optimize the 
 *      memory usage. 
 *
 *  @param[in]  name
 *      Name of the super heap
 *  @param[in]  memberHeaps
 *      Array of heaps which will be managed by the super heaps.
 *  @param[in]  numMemberHeaps
 *      Number of member heaps which need to be monitored by the Super Heap.
 *  @param[out]  errCode
 *      Error code populated if there was an error else 0
 *
 *  @sa
 *      Pktlib Error Codes
 *
 *  @retval
 *      Success -   Super Heap Handle
 *  @retval
 *      Error   -   NULL 
 */
Pktlib_HeapHandle Pktlib_createSuperHeap
(
    const char*         name,
    Pktlib_HeapHandle   memberHeaps[], 
    int32_t             numMemberHeaps,
    int32_t*            errCode
)
{
    int32_t         index;
    Pktlib_Heap*    ptrSuperHeap;

    /* Basic Validations: */
    if (name == NULL)
    {
        *errCode = PKTLIB_EINVAL;
        return NULL;
    }

    /* Super heaps are useful only if we have @ least 2 heaps to manage. 
     * There is no point in executing with just 1 heap. Similarly we cannot 
     * exceed the MAX permissible */
    if ((numMemberHeaps < 2) || (numMemberHeaps > PKTLIB_MAX_SUPER_MANAGED_HEAPS))
    {
        *errCode = PKTLIB_EINVAL;
        return NULL;
    }

    /* Cycle through and find a super heap entry */
    for (index = 0; index < PKTLIB_MAX_PACKET_HEAP; index++)
    {
        /* If the name is NULL implies that the heap is not being used. */
        if (gSuperPktHeaps[index].name[0] == 0x0)
            break;
    }

    /* Did we get an entry? */
    if (index == PKTLIB_MAX_PACKET_HEAP)
    {
        *errCode = PKLIB_EMAXHEAPLIMIT;
        return NULL;
    }

    /* We can create the super heap. Get the pointer to it. */
    ptrSuperHeap = &gSuperPktHeaps[index];

    /* Initialize the super heap */
    memset ((void *)ptrSuperHeap, 0, sizeof(Pktlib_Heap));

    /* Populate the super heap with the necessary information. */
    strncpy(ptrSuperHeap->name, name, PKTLIB_MAX_HEAP_NAME-1);

    /* Mark this as a super heap. */
    ptrSuperHeap->isSuperHeap = 1;

    /* Copy over the member heap handles */
    for (index = 0; index < numMemberHeaps; index++)
        ptrSuperHeap->memberHeapHandles[index] = memberHeaps[index];

    /* We need to now sort the heaps on the data buffer size */
    Pktlib_sortOnDataBufferSize(ptrSuperHeap->memberHeapHandles, numMemberHeaps);

#ifdef DEBUG_PKTLIB
    /* Debug: Dump the result of the sort. */
    for (index = 0; index < numMemberHeaps; index++)
        System_printf ("Debug: Member Heap %d has buffer size of %d bytes\n",
                        index, Pktlib_getMaxBufferSize(ptrSuperHeap->memberHeapHandles[index])); 
#endif

    /* There was no error; super heap has been created successfully. */
    *errCode = 0;
    return ptrSuperHeap;
}

/**
 *  @b Description
 *  @n  
 *      The function invalidates the cache contents of the packet. This needs
 *      to be called by the application if the packets are located in shared or external
 *      memory. If there are mutiple chained packets then the application needs to cycle
 *      through the chain and invalidate all the individual packets.
 *
 *  @param[in]  pPkt
 *      Packet to be invalidated
 *
 *  @retval
 *      Not applicable.
 */
void Pktlib_invalidatePkt(Ti_Pkt* pPkt)
{
    Pktlib_Heap*    ptrPktHeap;

    /* Get the pointer to the heap */
    ptrPktHeap = (Pktlib_Heap*)Pktlib_getPktHeap(pPkt);

    /* Invalidate the packet. */
    Pktlib_osalBeginPktAccess (ptrPktHeap, pPkt, ptrPktHeap->descSize);
}

/**
 *  @b Description
 *  @n  
 *      The function writeback the cache contents of the packet. This needs
 *      to be called by the application if the packets are located in shared or external
 *      memory. If there are mutiple chained packets then the application needs to cycle
 *      through the chain and invalidate all the individual packets.
 *
 *  @param[in]  pPkt
 *      Packet to be written back
 *
 *  @retval
 *      Not applicable.
 */
void Pktlib_writebackPkt(Ti_Pkt* pPkt)
{
    Pktlib_Heap*    ptrPktHeap;

    /* Get the pointer to the heap */
    ptrPktHeap = (Pktlib_Heap*)Pktlib_getPktHeap(pPkt);

    /* Writeback the packet. */
    Pktlib_osalEndPktAccess (ptrPktHeap, pPkt, ptrPktHeap->descSize);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to find a previously created heap using the name
 *
 *  @param[in]  name
 *      Name of the heap
 *
 *  @retval
 *      Success -   Heap Handle 
 *  @retval
 *      Error   -   NULL
 */
Pktlib_HeapHandle Pktlib_findHeapByName (const char* name)
{
    uint16_t    heapIndex;

    /* Cycle through all the private heaps */
    for (heapIndex = 0; heapIndex < PKTLIB_MAX_PACKET_HEAP; heapIndex++)
    {
        /* Heap was being used; match the heap name */
        if (strncmp(gPktHeaps[heapIndex].name, name, PKTLIB_MAX_HEAP_NAME) == 0)
            return &gPktHeaps[heapIndex];
    }

    /* Search all the shared heaps which exists in the system */
    for (heapIndex = 0; heapIndex < PKTLIB_MAX_PACKET_HEAP; heapIndex++)
    {
        /* Invaldiate the cache contents for the shared heap. */
        Pktlib_osalBeginMemAccess(&gSharedPktHeaps[heapIndex], sizeof(Pktlib_Heap));

        /* Heap was being used; match the heap name */
        if (strncmp(gSharedPktHeaps[heapIndex].name, name, PKTLIB_MAX_HEAP_NAME) == 0)
            return &gSharedPktHeaps[heapIndex];
    }

    /* Cycle through all the super heaps */
    for (heapIndex = 0; heapIndex < PKTLIB_MAX_PACKET_HEAP; heapIndex++)
    {
        /* Heap was being used; match the heap name */
        if (strncmp(gSuperPktHeaps[heapIndex].name, name, PKTLIB_MAX_HEAP_NAME) == 0)
            return &gSuperPktHeaps[heapIndex];
    }    

    /* Control comes here implying that there was no match */
    return NULL;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the stats of the specified heap.
 *
 *  @param[in]  heapHandle
 *      Handle of the heap for which the statistics are requested.
 *  @param[out] ptrHeapStats
 *      Heap Statistics populated by this API.
 *
 *  @retval
 *      Not Applicable.
 */
void Pktlib_getHeapStats
(
    Pktlib_HeapHandle   heapHandle, 
    Pktlib_HeapStats*   ptrHeapStats
)
{
    Pktlib_Heap*   ptrPktHeap = (Pktlib_Heap*)heapHandle;

    /* Get the snapshot of the stats. */
    ptrHeapStats->numFreeDataPackets   = Qmss_getQueueEntryCount(ptrPktHeap->freeQueueHnd);
    ptrHeapStats->numPacketsinGarbage  = Qmss_getQueueEntryCount(ptrPktHeap->garbageQueueHnd);

    /* Zero Queue stats are retreived only if the heap supports the mode. */
    if (ptrPktHeap->freeZeroQueueHnd)
        ptrHeapStats->numZeroBufferPackets = Qmss_getQueueEntryCount(ptrPktHeap->freeZeroQueueHnd);
    else
        ptrHeapStats->numZeroBufferPackets = 0;

    /* Check if the heap support data thresholds? */
    if (ptrPktHeap->dataBufferPktThreshold)
        ptrHeapStats->dataBufferThresholdStatus   = Qmss_getQueueThresholdStatus(ptrPktHeap->freeQueueHnd);
    else
        ptrHeapStats->dataBufferThresholdStatus   = 0;

    /* Check if the heap support zero buffer thresholds? */
    if (ptrPktHeap->zeroBufferPktThreshold)
        ptrHeapStats->zeroDataBufferThresholdStatus   = Qmss_getQueueThresholdStatus(ptrPktHeap->freeZeroQueueHnd);
    else
        ptrHeapStats->zeroDataBufferThresholdStatus   = 0;

    /* Check if the heap supports starvation queues */
    if (ptrPktHeap->useStarvationQueue)
    {
        /* Yes: Extract the starvation counters. */
        Pktlib_getQueueStarvationCount(ptrPktHeap, &ptrHeapStats->dataBufferStarvationCounter, 
                                       &ptrHeapStats->zeroDataBufferStarvationCounter);
    }
    else
    {
        /* No simply reset the starvation counters. */
        ptrHeapStats->dataBufferStarvationCounter = 0;
        ptrHeapStats->zeroDataBufferStarvationCounter = 0;
    }
    return;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to cleanup a packet.
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *  @param[in]  followLinks
 *      Flag which indicates if the links in the packet need to be
 *      followed.
 *
 *  @retval
 *      Not applicable
 */
static void __Pktlib_freePacket(Ti_Pkt* pPkt, uint8_t followLinks)
{
    Ti_Pkt*         pDonorPacket;
    Ti_Pkt*         pNextPacket;
    Pktlib_Info*    pPktLibInfo;
    uint8_t         refCount;
    void*           csHandle;
    Pktlib_Heap*    ptrPktHeap;

    /* Cycle through all the packets which have been chained together. */
    while (pPkt != NULL)
    {
        /* Get the library information for the packet. */
        pPktLibInfo = Pktlib_getPktLibInfo(pPkt);

        /* Get the next packet only if we need to follow the links */
        if (followLinks == 1)
            pNextPacket = Pktlib_getNextPacket(pPkt);
        else
            pNextPacket = NULL;

        /* Is the packet cloned or not? */
        if (Pktlib_getPktLibFlags(pPktLibInfo) & PKTLIB_CLONE_PACKET)
        {
            /* Cloned Packet: Can the packet get cleaned up or are there referenced held for the cloned
             * packet also? */
            ptrPktHeap = (Pktlib_Heap*)Pktlib_getPktHeap(pPkt);
            csHandle   = Pktlib_osalEnterCriticalSection(ptrPktHeap);
            refCount   = Pktlib_getRefCount(pPkt);
            if (refCount != 0)
                Pktlib_decRefCount(pPkt);
            Pktlib_osalExitCriticalSection(ptrPktHeap, csHandle);

            /* Can the cloned packet be deleted? */
            if (refCount == 0)
            {
				/* YES. We are trying to free a cloned packet. Get the information about the donor packet. */
				pDonorPacket = pPktLibInfo->donor;

				/* Ensure that we reset the data buffer and length to be 0 for cloned packets. */
				Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc*)pPkt, NULL, 0);

				/* Cleanup the cloned packet: Ensure that we reset the cloning flag. */
				Pktlib_setPktLibFlags (pPktLibInfo, Pktlib_getPktLibFlags(pPktLibInfo) & ~PKTLIB_CLONE_PACKET);
				pPktLibInfo->donor = 0;

				/* Kill the links. */
				Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)pPkt, NULL);

				/* Packet is being cleaned up. Clean it up depending on the type of the packet. */
				if (Pktlib_isDataBufferPkt(pPkt) == 1)
				{
					/* BUFFER Packet is being cleaned up. BUFFER packets can never be cloned only
					 * ZERO Buffer packets are cloned. So this implies that there is an error in the
					 * packet library. */
					while (1);
				}
				else
				{
					/* ZERO Buffer Packet is to be cleaned. Get the pointer to the heap */
					ptrPktHeap = (Pktlib_Heap*)Pktlib_getPktHeap(pPkt);

					/* Set the return queue for ZERO Buffer packets to be the garbage queue. This
					 * could have been overriden by applications. */
					Pktlib_cppiSetReturnQueue(Cppi_DescType_HOST, (Cppi_Desc*)pPkt, &ptrPktHeap->garbageQueueInfo);

					/* CACHE Fix: Writeback the packet. */
					Pktlib_writebackPkt(pPkt);

					/* Push the packet to the ZERO Free queue. */
					Qmss_queuePushDesc (ptrPktHeap->freeZeroQueueHnd, (void*)pPkt);
				}
            }
            else
            {
            	/* NO. References to the cloned packet are still being held so this cannot be deleted. We need to
            	 * skip this packet & proceed to the next linked packet. */
            	pDonorPacket = NULL;

				/* CACHE Fix: Writeback the packet, because ref count has been decreased. */
				Pktlib_writebackPkt(pPkt);
            }
        }
        else
        {
            /* We are trying to free an orignal packet. In this case we are the donor itself */
            pDonorPacket = pPkt;
        }

        /* Do we need to clean this packet or not? */
        if (pDonorPacket != NULL)
        {
			/* Critical Section: Decrement the reference counter of the donor packet
			 * if somebody was holding a reference using the heap interface table. */
			ptrPktHeap = (Pktlib_Heap*)Pktlib_getPktHeap(pDonorPacket);
			refCount   = Pktlib_getRefCount(pDonorPacket);
			if (refCount != 0)
			{
				csHandle   = Pktlib_osalEnterCriticalSection(ptrPktHeap);
				refCount   = Pktlib_getRefCount(pDonorPacket);
				if (refCount != 0)
					Pktlib_decRefCount(pDonorPacket);
				Pktlib_osalExitCriticalSection(ptrPktHeap, csHandle);
			}
			/* If there are no references to the DONOR packet we can clean this immediately. */
			if (refCount == 0)
			{
				/* Kill the links. */
				Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc *)pDonorPacket, NULL);

				/* Reset the data buffer with the orignal data buffer information. */
				((Cppi_HostDesc*)pDonorPacket)->buffPtr = ((Cppi_HostDesc*)pDonorPacket)->origBuffPtr;

				/* Determine the packet type */
				if (Pktlib_isDataBufferPkt(pDonorPacket) == 1)
				{
					/* BUFFER Packet is being cleaned up. Ensure that the return queue for
					 * these packets is setup to point to the heap free queue. These could have
					 * been modified to point to the garbage queue. */
					Pktlib_cppiSetReturnQueue(Cppi_DescType_HOST, (Cppi_Desc*)pDonorPacket, &ptrPktHeap->freeQueueInfo);

					/* CACHE Fix: Writeback the packet. */
					Pktlib_writebackPkt(pDonorPacket);

					/* Push the packet into the corresponding free queue */
					Qmss_queuePushDesc (ptrPktHeap->freeQueueHnd, (void*)pDonorPacket);
				}
				else
				{
					/* ZERO BUFFER Packet is being cleaned up. Here we ensure that the packet
					 * is placed into the free zero buffer queue. We dont need to set the return
					 * queue since for ZERO Buffer packets it is always configured to be
					 * the garbage queue.
					 *
					 * Control comes here only when a zero buffer is allocated and is freed up
					 * without using it. For example: In the case of split with the split size
					 * being aligned on packet boudary. */

					/* CACHE Fix: Writeback the packet, because ref count could be decreased*/
					Pktlib_writebackPkt(pDonorPacket);
					Qmss_queuePushDesc (ptrPktHeap->freeZeroQueueHnd, (void*)pDonorPacket);
				}
			}
			else
			{
				/* CACHE Fix: Writeback the packet, because ref count could be decreased*/
				Pktlib_writebackPkt(pDonorPacket);
			}
        }

        /* Goto the next packet. */
        pPkt = pNextPacket;
    }
    return;
}

/**
 *  @b Description
 *  @n  
 *      The function is the garbage collector for a specific heap. Packets
 *      which are cloned or split and then passed down to the IP blocks for 
 *      transmission could end up in the heap free queue(s) and would be 
 *      available for subsequent allocation; this might result in 
 *      unpredictable behavior because the clones are still in use. Hence 
 *      packets which have a non-zero reference count have their return
 *      queue modified to a garbage collection queue. The API needs to be
 *      called periodically to ensure that packets are moved back to the
 *      free queues from the garbage collection queue. 
 *
 *  @param[in]  heapHandle
 *      Handle of the heap on which the garbage collector is executed.
 *
 *  @retval
 *      Not Applicable.
 */
void Pktlib_garbageCollection(Pktlib_HeapHandle heapHandle)
{
    Ti_Pkt*        ptrPkt;
    Pktlib_Heap*   ptrPktHeap = (Pktlib_Heap*)heapHandle;

    /* Validations: Ensure a valid heap handle was passed */
    if (ptrPktHeap == NULL)
        return;

    /* Pop packets of the garbage queue */
    ptrPkt = (Ti_Pkt*)QMSS_DESC_PTR(Qmss_queuePop(ptrPktHeap->garbageQueueHnd));
    while (ptrPkt != NULL)
    {
    	/* Invalidate the packet. */
	    Pktlib_osalBeginPktAccess (ptrPktHeap, ptrPkt, ptrPktHeap->descSize);

        /* Cleanup the packet: We dont follow the links here because these packets 
         * have been placed here by the CPDMA which has placed all the chained packets
         * into their individual return queues but has NOT killed the links within 
         * the descriptor. */
        __Pktlib_freePacket(ptrPkt, 0);

        /* Get the next packet. */
        ptrPkt = (Ti_Pkt*)QMSS_DESC_PTR(Qmss_queuePop(ptrPktHeap->garbageQueueHnd));
    }
    return;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to cleanup a packet.
 *
 *  @param[in]  pPkt
 *      Pointer to the packet.
 *
 *  @retval
 *      Not applicable
 */
void Pktlib_freePacket(Ti_Pkt* pPkt)
{
    /* Cleanup the packet: This is called by the software to clean the packet
     * so here we need to follow the links and place all the chained packets into
     * the correct heap free queues. */
    __Pktlib_freePacket (pPkt, 1);
    return;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the MAX data size associated with a heap.
 *      During heap creation the size of the data buffers associated with the
 *      heap are specified. The function returns the size allocated. If the heap
 *      was created with no data buffers (only with zero data buffers) the 
 *      function will return 0 else it will return the size of the data buffer.
 *
 *  @param[in]  heapHandle
 *      Handle of the heap from where the packet is to be allocated.
 *
 *  @retval
 *      Size of the data buffer.
 */
uint32_t Pktlib_getMaxBufferSize(Pktlib_HeapHandle heapHandle)
{
    Pktlib_Heap* ptrPktHeap;

    /* Get the pointer to the heap. */
    ptrPktHeap = (Pktlib_Heap *)heapHandle;

    /* Ensure that a valid heap handle was passed. */
    if (ptrPktHeap == NULL)
        return 0;

    /* Return the size of the data buffer. */
    return ptrPktHeap->dataBufferSize;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to allocate a packet of the specified size from the
 *      super heap. If the size passed is 0; the function allocates a packet with no
 *      buffer i.e. bufferless packet. If the size passed is greater than the 
 *      size of the data packet in the heap then the allocation will fail.
 *
 *  @param[in]  ptrSuperHeap
 *      Pointer to the super heap from which the packet is to be allocated
 *  @param[in]  size
 *      Size of the packet which is to be allocated.
 *
 *  @retval
 *      Success -   Pointer to the packet
 *  @retval
 *      Error   -   NULL
 */
static Ti_Pkt* Pktlib_superHeapAllocPacket(Pktlib_Heap* ptrSuperHeap, uint32_t size)
{
    uint32_t    index;
    Ti_Pkt*     ptrPkt = NULL;

    /* Cycle through all the member heaps */
    for (index = 0; index < PKTLIB_MAX_SUPER_MANAGED_HEAPS; index++)
    {
        /* Allocate the packet using the member heap */
        ptrPkt = Pktlib_allocPacket(ptrSuperHeap->memberHeapHandles[index], size);
        if (ptrPkt != NULL)
            return ptrPkt;

        /* Allocation failed; this could happen for a number of reasons
         *  (1) The data buffer size requested was greater than the heap buffer size 
         *  (2) There were no packets available in the heap 
         * For either of these cases we should basically just move on and try and use
         * the next member heap */
        continue;
    }

    /* Control comes here implies that the SUPER Heap was NOT able to satisfy the request */
    return NULL;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to allocate a packet of the specified size from the
 *      heap. If the size passed is 0; the function allocates a packet with no
 *      buffer i.e. bufferless packet. If the size passed is greater than the 
 *      size of the data packet in the heap then the allocation will fail.
 *
 *  @param[in]  heapHandle
 *      Handle of the heap from where the packet is to be allocated.
 *  @param[in]  size
 *      Size of the packet which is to be allocated.
 *
 *  @retval
 *      Success -   Pointer to the packet
 *  @retval
 *      Error   -   NULL
 */
Ti_Pkt* Pktlib_allocPacket(Pktlib_HeapHandle heapHandle, uint32_t size)
{
    Pktlib_Heap*    ptrPktHeap;
    Ti_Pkt*         ptrPkt;
    uint8_t*        ptrDataBuffer;
    uint32_t        dataLen;
    Qmss_QueueHnd   queueHandle;

    /* Get the pointer to the heap. */
    ptrPktHeap = (Pktlib_Heap *)heapHandle;

    /* Ensure that a valid heap handle was passed. */
    if (ptrPktHeap == NULL)
        return NULL;

    /* Is this a Super heap? */
    if (ptrPktHeap->isSuperHeap == 1)
        return Pktlib_superHeapAllocPacket(ptrPktHeap, size);

    /* Check the size to determine the type of allocation? 
     *  - Use the Zero Queue if the size is 0.
     *  - Use the data queue if the size is within the heap size. 
     *  - Else the heap cannot satisfy the request */
    if (size == 0)
    {
        /* Zero Buffer Allocation: */
        queueHandle = ptrPktHeap->freeZeroQueueHnd;
    } 
    else if (size <= ptrPktHeap->dataBufferSize)
    {
        /* Normal Buffer Allocation: */
        queueHandle = ptrPktHeap->freeQueueHnd;
    }
    else
    {
        /* Error: Request cannot be satisfied. */
        queueHandle = 0;
    }

    /* Allocation can proceed only if a valid queue handle was present. 
     *  - This is a catch all because we can have a zero buffer allocation
     *    being attempted on a heap which was not configured for zero buffers */
    if (queueHandle == 0)
        return NULL;

    /* Pop off a packet from the queue. */
#ifdef _VIRTUAL_ADDR_SUPPORT
    ptrPkt = (Ti_Pkt*)QMSS_DESC_PTR(Pktlib_osalPhyToVirt(Qmss_queuePopRaw(queueHandle)));
#else
    ptrPkt = (Ti_Pkt*)QMSS_DESC_PTR(Qmss_queuePop(queueHandle));
#endif

    /* Did we get a packet. */
    if (ptrPkt != NULL)
    {
        /* Yes. Invalidate the packet. */
        Pktlib_osalBeginPktAccess (ptrPktHeap, ptrPkt, ptrPktHeap->descSize);

#ifdef _VIRTUAL_ADDR_SUPPORT
        Cppi_HostDesc*  ptrDesc = (Cppi_HostDesc *)ptrPkt;
        if (ptrDesc->buffPtr)
        {
            ptrDesc->buffPtr = (uint32_t)Pktlib_osalPhyToVirt((void *)(ptrDesc->buffPtr));
            if (!(ptrDesc->buffPtr)) return (void *)0;
        }

        if (ptrDesc->origBuffPtr)
        {
            ptrDesc->origBuffPtr = (uint32_t)Pktlib_osalPhyToVirt((void *)(ptrDesc->origBuffPtr));
            if (!(ptrDesc->origBuffPtr)) return (void *)0;
        }
#endif

        /* Get the original buffer information. */
        Cppi_getOriginalBufInfo (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt,
                                 (uint8_t**)&ptrDataBuffer, &dataLen);

        /* Ensure that the original buffer and buffer address is one and the same. */
        Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt, (uint8_t*)ptrDataBuffer, size);

        /* Kill any other links to the packets. */
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt, NULL);

        /* Setup the packet length  */
        Pktlib_setPacketLen(ptrPkt, size);
    }

    /* Return the allocated packet. */
    return ptrPkt;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to merge packets. The function chains "Packet2"
 *      to the end of "Packet1".
 *
 *  @param[in]  pPkt1
 *      Packet1 which is to be merged at the beginning of packet2
 *  @param[in]  pPkt2
 *      Packet2 which is merged at the end of packet1
 *  @param[in]  pLastPkt
 *      Optional parameter to the last element of the Packet1 chain. If this
 *      is known then packet2 is chained to the end of this packet; else the
 *      API will try and determine the last element in the Packet1 chain
 *
 *  @retval
 *      Merged packet
 */
Ti_Pkt* Pktlib_packetMerge(Ti_Pkt* pPkt1, Ti_Pkt* pPkt2, Ti_Pkt* pLastPkt)
{
    Cppi_HostDesc*  ptrPrevDesc = NULL;
    Cppi_HostDesc*  ptrDesc;
	uint32_t        packetLength;

    /* Validations: Ensure that the packets passed to be merged are correct */
    if ((pPkt1 == NULL) || (pPkt2 == NULL))
        return NULL;

    /* Is the last packet in the chain specified? */
    if (pLastPkt == NULL)
    {
        /* NO. Get the pointer to the descriptor. */
        ptrDesc = (Cppi_HostDesc*)Cppi_getNextBD(Cppi_DescType_HOST, (Cppi_Desc *)pPkt1);
        while (ptrDesc != NULL)
        {
            /* Store the previous descriptor. */
            ptrPrevDesc = ptrDesc;

            /* Get the pointer to the next descriptor. */
            ptrDesc = (Cppi_HostDesc*)Cppi_getNextBD(Cppi_DescType_HOST, (Cppi_Desc *)ptrDesc);
        }
    }
    else
    {
        /* YES. Get the last element in the chain. */
        ptrPrevDesc = (Cppi_HostDesc*)pLastPkt;
    }

    /* Link the last buffer descriptor of packet1 with the packet2. If packet1 had only 1 
     * descriptor we link the head descriptor of packet1 with packet2 else we link the
     * last descriptor from packet1 to the head descriptor of packet2 */
    if (ptrPrevDesc == NULL)
    {
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc*)pPkt1, (Cppi_Desc*)pPkt2);
    }
    else
    {
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc*)ptrPrevDesc, (Cppi_Desc*)pPkt2);
        Pktlib_writebackPkt((Ti_Pkt*)ptrPrevDesc);
    }

	/* Compute the total packet length after merging the packets. */
    packetLength = Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pPkt1) + 
                   Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pPkt2);

    /* Ensure that the packet length is properly setup in the packet1 to account for the total length */
    Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pPkt1, packetLength);
    Pktlib_writebackPkt(pPkt1);

    /* Reset the packet length in the packet2 because this has now been merged and the 
     * length has already been accounted for */
    Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pPkt2, 0);
    Pktlib_writebackPkt(pPkt2);

    /* Return the merged packet. */
	return (Ti_Pkt*)(pPkt1);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to clone a packet. 
 *
 *  @param[in]  ptrPktOrig
 *      Orignal Packet which is to be cloned.
 *  @param[in]  ptrClonePacket
 *      The pointer to the cloned packet which is a list of packets with 
 *      zero buffers. There should be the same number of buffers as the 
 *      orignal packet.
 *
 *  @retval
 *      0   -   Success
 *  @retval
 *      -1  -   Error
 */
int32_t Pktlib_clonePacket(Ti_Pkt* ptrPktOrig, Ti_Pkt* ptrClonePacket)
{
    Pktlib_Info*    pOrigPktLibInfo;
    Pktlib_Info*    pClonePktLibInfo;
    uint8_t*        ptrDataBuffer;
    uint32_t        dataLen;
	uint32_t        packetLength;
    Pktlib_Heap*    ptrPktHeap;
    void*           csHandle;
    uint8_t         refCount;

    /* Get the packet length of the orignal packet. */
    packetLength = Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)ptrPktOrig);

    /* Ensure that the cloned packet inherits the same packet length. */
    Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)ptrClonePacket, packetLength);

    /* Cycle through the chain of packets */
    while (ptrPktOrig != NULL)
    {
        /* We should always have a packet in the cloned chain. If not we are in trouble because
         * we will not be able to clone the packet. */
        if (ptrClonePacket == NULL)
            return -1;

        /* Get the packet lib info of the orignal & cloned packet. */
        pOrigPktLibInfo  = Pktlib_getPktLibInfo(ptrPktOrig);
        pClonePktLibInfo = Pktlib_getPktLibInfo(ptrClonePacket);

        /* All cloned packets should be 0 buffer packets. If these are buffer packets then
         * the callee has made a mistake and we cannot proceed. */
        if (Pktlib_isDataBufferPkt(ptrClonePacket) == 1)
            return -1;

        /* Set the flags in the cloned packet appropriately. */
        Pktlib_setPktLibFlags(pClonePktLibInfo, Pktlib_getPktLibFlags(pClonePktLibInfo) | PKTLIB_CLONE_PACKET);

        /* Ensure that we configure the return queue for the orignal packet to be the 
         * garbage queue because we dont want these packets to be placed immediately
         * into the free queue (by the IP blocks) once they are done because we might have 
         * other references. Note: The return queue for cloned packets is the GARBAGE queue
         * by default. */
        Pktlib_setReturnQueueToGarbage(ptrPktOrig);

        /* Determine the donor of the packet: This depends upon the packet type of the orignal */
        if (Pktlib_getPktLibFlags(pOrigPktLibInfo) & PKTLIB_CLONE_PACKET)
        {
            /* The cloned packet is being cloned. So we set the cloned packet donor to 
             * the donor of the orignal packet. */
            pClonePktLibInfo->donor = pOrigPktLibInfo->donor;
        }
        else
        {
            /* The orignal packet is being cloned. So we set the cloned packet donor to 
             * the orignal packet. */
            pClonePktLibInfo->donor = ptrPktOrig;
        }

        /* Get the current buffer & buffer length of the orignal packet. */
        Cppi_getData (Cppi_DescType_HOST, (Cppi_Desc*)ptrPktOrig, &ptrDataBuffer, &dataLen);

        /* Set this in the cloned packet. */
        Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc*)ptrClonePacket, ptrDataBuffer, dataLen);

        /* Critical Section Start: Incrementing the reference counter. */
        ptrPktHeap = (Pktlib_Heap*)Pktlib_getPktHeap(ptrClonePacket);
        csHandle   = Pktlib_osalEnterCriticalSection(ptrPktHeap);

        /* Get the reference counter */
        refCount = Pktlib_getRefCount(pClonePktLibInfo->donor);

        /* Make sure we do NOT exceed the MAX allowed. */
        if (refCount < PKTLIB_MAX_REF_COUNT)
            Pktlib_incRefCount(pClonePktLibInfo->donor);

        /* Critical Section End: */
        Pktlib_osalExitCriticalSection(ptrPktHeap, csHandle);

        /* If we exceed the MAX reference count allowed there is no point continuing further */
        if (refCount >= PKTLIB_MAX_REF_COUNT)
            return -1;

        /* CACHE Fix: Writeback the original & cloned packets */
        Pktlib_writebackPkt(ptrPktOrig);
        Pktlib_writebackPkt(ptrClonePacket);

        /* Get the next packet (Orignal & Clone) */
        ptrPktOrig      = Pktlib_getNextPacket(ptrPktOrig);
        ptrClonePacket  = Pktlib_getNextPacket(ptrClonePacket);
    }

    /* Packet has been successfully cloned. */
    return 0;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to split the packet. Splitting a packet
 *      is done at the 'splitPacketSize' specified. The head of the
 *      split packet is returned in 'pPkt1' while the head of the 
 *      second split is returned in 'pPkt2'. 
 *
 *      The zero buffer packet is marked as a clone because it refers to
 *      the data buffer packet. Data buffer packets cannot be freed till
 *      all the clones using them have been freed. The goal should be to
 *      free the CLONED packets as soon as possible so that the data buffer
 *      packets are not being held. 
 *
 *      This variant of the function splits the packet and sets the zero
 *      buffer packet (i.e. Cloned Packet) to point to the data *before* the
 *      split size and so it is returned in the pPkt1. So use this function
 *      if the pPkt1 is getting cleaned before pPkt2.
 *
 *  @sa
 *      Pktlib_splitPacket2
 *
 *  @param[in]  pOrgPkt
 *      Pointer to the Orignal packet which is to be split. 
 *  @param[in]  pNewPkt
 *      Pointer to the packet which has no buffer attached to it.
 *  @param[in]  splitPacketSize
 *      Size of the packet which is to be split
 *  @param[out] pPkt1
 *      The pointer to the first packet after the split.
 *  @param[out] pPkt2
 *      The pointer to the second packet after the split.
 *
 *  @retval
 *      0   -   Success (The packet passed was used)
 *  @retval
 *      1   -   Success (The packet passed was NOT used)
 *  @retval
 *      -1  -   Error
 */
int32_t Pktlib_splitPacket
(
    Ti_Pkt*     pOrgPkt,
    Ti_Pkt*     pNewPkt,
    uint32_t    splitPacketSize,
    Ti_Pkt**    pPkt1,
    Ti_Pkt**    pPkt2
)
{
    uint32_t    packetLength;
    Ti_Pkt*     pSplitPkt;
    Ti_Pkt*     pPrevSplitPkt = NULL;
    uint8_t*    ptrDataBuffer;

    /* Ensure that valid parameters were passed to the split API */
    if ((pOrgPkt == NULL) || (pNewPkt == NULL))
        return -1;

    /* Get the packet length of the packet which is to be split. */
    packetLength = Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pOrgPkt);

    /* Validations: We cannot split a packet if the packet length is less than the split size */
    if (packetLength < splitPacketSize)
        return -1;

    /* If we are trying to split the packet into the same size as the current length 
     * than this function is a no operation. Our work is already done */
    if (packetLength == splitPacketSize)
    {
        /* The size is the same. So the split packet1 is the same as the orignal packet
         * while the split packet2 is the NULL. */
        *pPkt1 = pOrgPkt;
        *pPkt2 = NULL;

        /* We have not used the packet which was passed to us in this case. */
        return 1;
    }

    /* Cycle through the descriptors till we can determine where the split has to be done. */
    pSplitPkt    = pOrgPkt;
    packetLength = 0;
    while (pSplitPkt != NULL)
    {
        /* Get the data buffer length. */
        packetLength = packetLength + Cppi_getDataLen (Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt);

        /* Have we reached the split length specified? */
        if (packetLength >= splitPacketSize)
            break;

        /* Remember the previous split packet. */
        pPrevSplitPkt = pSplitPkt;

        /* Get the next packet. */
        pSplitPkt = Pktlib_getNextPacket(pSplitPkt);
    }

    /* Did we break out if we have reached the end of the chain of packets. This case should not occur
     * the only possibility for this is that the packet length in the packet was messed up. */
    if (pSplitPkt == NULL)
        return -1;

    /* Handle the following cases:
     *  a) The split is at the end of the data buffer in the split packet
     *  b) The split is in the middle of the data buffer in the split packet. */
    if (packetLength == splitPacketSize)
    {
        /* This is case (a) above. Update the links to the split packets. */
        *pPkt1 = pOrgPkt;
        *pPkt2 = Pktlib_getNextPacket(pSplitPkt);

        /* Kill the next link in the split packet. */
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt, (Cppi_Desc*)NULL);
        Pktlib_writebackPkt(pSplitPkt);

        /* Update the packet lengths in both the split packets. 
         *  The split packet1 has a packet length which is the size specified in the API.
         *  The second split packet has whatever is left. */
        packetLength = Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pOrgPkt) - splitPacketSize;
        Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)(*pPkt2), packetLength);
        Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)(*pPkt1), splitPacketSize);

        /* Writeback the packets. */
        Pktlib_writebackPkt(*pPkt1);
        Pktlib_writebackPkt(*pPkt2);

        /* The split operation was successful but we did not use the packet which was passed to us. */
        return 1;
    }

    /* This is case (b). 
     *  Start populating the new packet which was passed to us. */
    {
        uint32_t        dataBufferLen1;
        uint32_t        dataBufferLen2;
        Ti_Pkt*         ptrDonorPkt;
        uint32_t        orgDataBufferLen;
        Pktlib_Info*    pNewPktLibInfo;
        Pktlib_Info*    pSplitPktLibInfo;
        Pktlib_Heap*    ptrPktHeap;
        void*           csHandle;
        uint8_t         refCount;

        /* Get the heap to which the new packet belongs. */
        ptrPktHeap = (Pktlib_Heap*)Pktlib_getPktHeap(pNewPkt);

        /* Get the packet lib info of the orignal & cloned packet. */
        pSplitPktLibInfo  = Pktlib_getPktLibInfo(pSplitPkt);
        pNewPktLibInfo    = Pktlib_getPktLibInfo(pNewPkt);        

        /* Determine the donor of the packet: This depends upon the packet type of the orignal */
        if (Pktlib_getPktLibFlags(pSplitPktLibInfo) & PKTLIB_CLONE_PACKET)
        {
            /* The cloned packet is being cloned. So we set the cloned packet donor to 
             * the donor of the orignal packet. */
            ptrDonorPkt = pSplitPktLibInfo->donor;
        }
        else
        {
            /* The orignal packet is being cloned. So we set the cloned packet donor to 
             * the orignal packet. */
            ptrDonorPkt = pSplitPkt;
        }

        /* Check the reference counter of the DONOR? This is required because if we are exceeding
         * the maximum value of this counter then we cannot split the packet anymore. However if
         * we are able to proceed; then we need to take the critical section and do all the other
         * operations atomically. */
        csHandle = Pktlib_osalEnterCriticalSection(ptrPktHeap);
        refCount = Pktlib_getRefCount(ptrDonorPkt);        

        /* Can we proceed with the split? */
        if (refCount < PKTLIB_MAX_REF_COUNT)
        {
            /* YES. Increment the reference counter and release the critical section. */
            Pktlib_incRefCount(ptrDonorPkt);

            /* Release the critical section. */
            Pktlib_osalExitCriticalSection(ptrPktHeap, csHandle);

            /* We have now determined the descriptor on which the split will be done. Now we need
             * to determine the location in the buffer where the split is done. */
            Cppi_getData (Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt, &ptrDataBuffer, &orgDataBufferLen);

            /* Determine the lengths of the 2 data buffers accounting for the split. */
            dataBufferLen1 = splitPacketSize  - (packetLength - orgDataBufferLen);
            dataBufferLen2 = orgDataBufferLen - dataBufferLen1;

            /* The Split packet is now broken into 2 parts; the first part of the buffer is referenced by the
             * Zero Buffer Packet; the second part is referenced as is by the Split Packet
             *
             * Initialize the Zero Buffer Packet appropriately. */
            Cppi_setData(Cppi_DescType_HOST, (Cppi_Desc*)pNewPkt, (uint8_t*)ptrDataBuffer, dataBufferLen1);

            /* Update the split packet appropriately. */
            Cppi_setData(Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt, (uint8_t*)(ptrDataBuffer + dataBufferLen1),
                         dataBufferLen2);

            /* Set the flags in the cloned packet appropriately. */
            Pktlib_setPktLibFlags(pNewPktLibInfo, Pktlib_getPktLibFlags(pNewPktLibInfo) | PKTLIB_CLONE_PACKET);

            /* Remember the DONOR. */
            pNewPktLibInfo->donor = ptrDonorPkt;

            /* Ensure that we configure the return queue for the split packet to be the
             * garbage queue because we dont want these packets to be placed immediately
             * into the free queue (by the IP blocks) once they are done because we might have
             * other references. Note: The return queue for 'new' packets is the GARBAGE queue
             * by default. */
            Pktlib_setReturnQueueToGarbage(pSplitPkt);
    
            /* Writeback all the packets which have been modified here */
            Pktlib_writebackPkt(ptrDonorPkt);
            Pktlib_writebackPkt(pNewPkt);
            Pktlib_writebackPkt(pSplitPkt);
        }
        else
        {
            /* NO. Release the critical section and return error. */
            Pktlib_osalExitCriticalSection(ptrPktHeap, csHandle);
            return -1;
        }
    }

    /* Get the packet length of the orignal packet. */
    packetLength = Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pOrgPkt);

    /* Link the Zero Buffer Descriptor. */
    if (pPrevSplitPkt != NULL)
    {
        /* Link the packet previous to the split packet with the new packet */
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc*)pPrevSplitPkt, (Cppi_Desc*)pNewPkt);
        Pktlib_writebackPkt(pPrevSplitPkt);
        
        /* Populate the split packet information to return back. */
        *pPkt1 = pOrgPkt;
        *pPkt2 = pSplitPkt;
    }
    else
    {
        /* Populate the split packet information to return back. */
        *pPkt1 = pNewPkt;
        *pPkt2 = pSplitPkt;
    }

    /* Set the packet length correctly in the returned packets. */
    Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)(*pPkt1), splitPacketSize);
    Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)(*pPkt2), (packetLength - splitPacketSize));

    /* Writeback the packets. */
    Pktlib_writebackPkt(*pPkt1);
    Pktlib_writebackPkt(*pPkt2);

    /* The packet has been split successfully. */
    return 0;    
}

/**
 *  @b Description
 *  @n  
 *      The function is used to split the packet. Splitting a packet
 *      is done at the 'splitPacketSize' specified. The head of the
 *      split packet is returned in 'pPkt1' while the head of the 
 *      second split is returned in 'pPkt2'. 
 *
 *      The zero buffer packet is marked as a clone because it refers to
 *      the data buffer packet. Data buffer packets cannot be freed till
 *      all the clones using them have been freed. The goal should be to
 *      free the CLONED packets as soon as possible so that the data buffer
 *      packets are not being held. 
 *
 *      This variant of the function splits the packet and sets the zero
 *      buffer packet (i.e. Cloned Packet) to point to the data *after* the
 *      split size and so it is returned in the pPkt2. So use this function
 *      if the pPkt2 is getting cleaned before pPkt1.
 *
 *  @sa
 *      Pktlib_splitPacket
 *
 *  @param[in]  pOrgPkt
 *      Pointer to the Orignal packet which is to be split. 
 *  @param[in]  pNewPkt
 *      Pointer to the packet which has no buffer attached to it.
 *  @param[in]  splitPacketSize
 *      Size of the packet which is to be split
 *  @param[out] pPkt1
 *      The pointer to the first packet after the split.
 *  @param[out] pPkt2
 *      The pointer to the second packet after the split.
 *
 *  @retval
 *      0   -   Success (The packet passed was used)
 *  @retval
 *      1   -   Success (The packet passed was NOT used)
 *  @retval
 *      -1  -   Error
 */
int32_t Pktlib_splitPacket2
(
    Ti_Pkt*     pOrgPkt,
    Ti_Pkt*     pNewPkt,
    uint32_t    splitPacketSize,
    Ti_Pkt**    pPkt1,
    Ti_Pkt**    pPkt2
)
{
    uint32_t    packetLength;
    Ti_Pkt*     pSplitPkt;
    uint8_t*    ptrDataBuffer;

    /* Ensure that valid parameters were passed to the split API */
    if ((pOrgPkt == NULL) || (pNewPkt == NULL))
        return -1;

    /* Get the packet length of the packet which is to be split. */
    packetLength = Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pOrgPkt);

    /* Validations: We cannot split a packet if the packet length is less than the split size */
    if (packetLength < splitPacketSize)
        return -1;

    /* If we are trying to split the packet into the same size as the current length 
     * than this function is a no operation. Our work is already done */
    if (packetLength == splitPacketSize)
    {
        /* The size is the same. So the split packet1 is the same as the orignal packet
         * while the split packet2 is the NULL. */
        *pPkt1 = pOrgPkt;
        *pPkt2 = NULL;

        /* We have not used the packet which was passed to us in this case. */
        return 1;
    }

    /* Cycle through the descriptors till we can determine where the split has to be done. */
    pSplitPkt    = pOrgPkt;
    packetLength = 0;
    while (pSplitPkt != NULL)
    {
        /* Get the data buffer length. */
        packetLength = packetLength + Cppi_getDataLen (Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt);

        /* Have we reached the split length specified? */
        if (packetLength >= splitPacketSize)
            break;

        /* Get the next packet. */
        pSplitPkt = Pktlib_getNextPacket(pSplitPkt);
    }

    /* Did we break out if we have reached the end of the chain of packets. This case should not occur
     * the only possibility for this is that the packet length in the packet was messed up. */
    if (pSplitPkt == NULL)
        return -1;

    /* Handle the following cases:
     *  a) The split is at the end of the data buffer in the split packet
     *  b) The split is in the middle of the data buffer in the split packet. */
    if (packetLength == splitPacketSize)
    {
        /* This is case (a) above. Update the links to the split packets. */
        *pPkt1 = pOrgPkt;
        *pPkt2 = Pktlib_getNextPacket(pSplitPkt);

        /* Kill the next link in the split packet. */
        Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt, (Cppi_Desc*)NULL);
        Pktlib_writebackPkt(pSplitPkt);

        /* Update the packet lengths in both the split packets. 
         *  The split packet1 has a packet length which is the size specified in the API.
         *  The second split packet has whatever is left. */
        packetLength = Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pOrgPkt) - splitPacketSize;
        Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)(*pPkt2), packetLength);
        Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)(*pPkt1), splitPacketSize);

        /* Writeback the packets. */
        Pktlib_writebackPkt(*pPkt1);
        Pktlib_writebackPkt(*pPkt2);

        /* The split operation was successful but we did not use the packet which was passed to us. */
        return 1;
    }

    /* This is case (b). 
     *  Start populating the new packet which was passed to us. */
    {
        uint32_t        dataBufferLen1;
        uint32_t        dataBufferLen2;
        Ti_Pkt*         ptrDonorPkt;
        uint32_t        orgDataBufferLen;
        Pktlib_Info*    pNewPktLibInfo;
        Pktlib_Info*    pSplitPktLibInfo;
        Pktlib_Heap*    ptrPktHeap;
        void*           csHandle;
        uint8_t         refCount;

        /* Get the heap to which the new packet belongs. */
        ptrPktHeap = Pktlib_getPktHeap(pNewPkt);

        /* Get the packet lib info of the orignal & cloned packet. */
        pSplitPktLibInfo  = Pktlib_getPktLibInfo(pSplitPkt);
        pNewPktLibInfo    = Pktlib_getPktLibInfo(pNewPkt);        

        /* Determine the donor of the packet: This depends upon the packet type of the orignal */
        if (Pktlib_getPktLibFlags(pSplitPktLibInfo) & PKTLIB_CLONE_PACKET)
        {
            /* The cloned packet is being cloned. So we set the cloned packet donor to 
             * the donor of the orignal packet. */
            ptrDonorPkt = pSplitPktLibInfo->donor;
        }
        else
        {
            /* The orignal packet is being cloned. So we set the cloned packet donor to 
             * the orignal packet. */
            ptrDonorPkt = pSplitPkt;
        }

        /* Check the reference counter of the DONOR? This is required because if we are exceeding
         * the maximum value of this counter then we cannot split the packet anymore. However if
         * we are able to proceed; then we need to take the critical section and do all the other
         * operations atomically. */
        csHandle = Pktlib_osalEnterCriticalSection(ptrPktHeap);
        refCount = Pktlib_getRefCount(ptrDonorPkt);        

        /* Can we proceed with the split? */
        if (refCount < PKTLIB_MAX_REF_COUNT)
        {
        	/* YES. Increment the reference counter and release the critical section. */
            Pktlib_incRefCount(ptrDonorPkt);

            /* Release the critical section. */
            Pktlib_osalExitCriticalSection(ptrPktHeap, csHandle);

            /* We have now determined the descriptor on which the split will be done. Now we need
             * to determine the location in the buffer where the split is done. */
            Cppi_getData (Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt, &ptrDataBuffer, &orgDataBufferLen);

            /* Determine the lengths of the 2 data buffers accounting for the split. */
            dataBufferLen1 = splitPacketSize  - (packetLength - orgDataBufferLen);
            dataBufferLen2 = orgDataBufferLen - dataBufferLen1;

            /* The Split packet is now broken into 2 parts; 
             *  - The first part of the packet is referred to by split packet
             *  - The second part of the packet is referred by the Zero Buffer Packet
             *
             * Initialize the Zero Buffer Packet appropriately. */
            Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc*)pNewPkt, (uint8_t*)(ptrDataBuffer + dataBufferLen1), dataBufferLen2);

            /* The zero buffer packet inherits all the links from the original split packet. */
            Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc*)pNewPkt, (Cppi_Desc*)Pktlib_getNextPacket(pSplitPkt));

            /* Set the data buffer length in the split packet descriptor correctly. */
            Cppi_setDataLen(Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt, dataBufferLen1);

            /* Kill the links in the split packet. */
            Cppi_linkNextBD (Cppi_DescType_HOST, (Cppi_Desc*)pSplitPkt, NULL);

            /* Set the flags in the cloned packet appropriately. */
            Pktlib_setPktLibFlags(pNewPktLibInfo, Pktlib_getPktLibFlags(pNewPktLibInfo) | PKTLIB_CLONE_PACKET);

            /* Remember the DONOR. */
            pNewPktLibInfo->donor = ptrDonorPkt;

            /* Ensure that we configure the return queue for the split packet to be the
             * garbage queue because we dont want these packets to be placed immediately
             * into the free queue (by the IP blocks) once they are done because we might have
             * other references. Note: The return queue for 'new' packets is the GARBAGE queue
             * by default. */
            Pktlib_setReturnQueueToGarbage(pSplitPkt);

            /* Writeback the packets which have been modified. */
            Pktlib_writebackPkt(ptrDonorPkt);
            Pktlib_writebackPkt(pNewPkt);
            Pktlib_writebackPkt(pSplitPkt);
        }
        else
        {
            /* NO. Release the critical section and return error. */
            Pktlib_osalExitCriticalSection(ptrPktHeap, csHandle);
            return -1;
        }
    }

    /* Get the packet length of the orignal packet. */
    packetLength = Cppi_getPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)pOrgPkt);

    /* The first packet always starts from the original packet. */
    *pPkt1 = pOrgPkt;

    /* The second packet is always the cloned packet which is after the split. */
    *pPkt2 = pNewPkt;

    /* Set the packet length correctly in the returned packets. */
    Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)(*pPkt1), splitPacketSize);
    Cppi_setPacketLen(Cppi_DescType_HOST, (Cppi_Desc*)(*pPkt2), (packetLength - splitPacketSize));

    /* Writeback the packets. */
    Pktlib_writebackPkt(*pPkt1);
    Pktlib_writebackPkt(*pPkt2);

    /* The packet has been split successfully. */
    return 0;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to return the internal heap queue where the 
 *      data buffer packets are stored.
 *
 *  @param[in]  heapHandle
 *      Heap handle.
 *
 *  @retval
 *      QMSS Queue Handle where buffer packets are stored. This could be 
 *      NULL if the heap was created with no buffer packets or if this was
 *      a SUPER Heap.
 */
Qmss_QueueHnd Pktlib_getInternalHeapQueue(Pktlib_HeapHandle heapHandle)
{
    Pktlib_Heap* ptrPktHeap;

    /* Get the pointer to the heap. */
    ptrPktHeap = (Pktlib_Heap *)heapHandle;

    /* This API is not supported on the Super Heap */
    if (ptrPktHeap->isSuperHeap == 1)
        return 0;

    /* Return the Data Buffer Queue. */
    return ptrPktHeap->freeQueueHnd;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to return the internal heap queue where the 
 *      zero buffer packets are stored.
 *
 *  @param[in]  heapHandle
 *      Heap handle.
 *
 *  @retval
 *      QMSS Queue Handle where zero packets are stored. This could be 
 *      NULL if the heap was created with no zero buffer packets or if
 *      this is a SUPER HEAP.
 */
Qmss_QueueHnd Pktlib_getZeroHeapQueue(Pktlib_HeapHandle heapHandle)
{
    Pktlib_Heap* ptrPktHeap;

    /* Get the pointer to the heap. */
    ptrPktHeap = (Pktlib_Heap *)heapHandle;

    /* This API is not supported on the Super Heap */
    if (ptrPktHeap->isSuperHeap == 1)
        return 0;

    /* Return the Zero Buffer Queue. */
    return ptrPktHeap->freeZeroQueueHnd;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the version information of the packet library
 *
 *  @retval
 *      Version Information.
 */
uint32_t Pktlib_getVersion (void)
{
    return PKTLIB_VERSION_ID;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to delete the heap. Please ensure that the
 *      following criteria are met:-
 *
 *      @verbatim
       a) Once the heap is deleted it is  the responsibility of the 
          application NOT to use the heap for allocations; there is no 
          run time checking added in the PKTLIB API to determine if 
          the heap is operational or not since this will cause 
          performance penalties.
       b) The heap can only be deleted by the core which created the heap
          This needs to be taken care for "shared heaps". This is NOT 
          enforced in the API but is the responsibility of the application.
        @endverbatim
 *
 *  @param[in]  heapHandle
 *      Heap handle to be deleted
 *  @param[out]  errCode
 *      Error Code populated if there is an error.
 *
 *  @retval
 *      0   -   Success
 *  @retval
 *      <0  -   Error
 */
int32_t Pktlib_deleteHeap (Pktlib_HeapHandle heapHandle, int32_t* errCode)
{
    Pktlib_Heap*    ptrPktHeap;
    Ti_Pkt*         ptrPkt;
    uint8_t*        ptrDataBuffer;
    uint32_t        dataBufferLen;
    int32_t         memoryRegionQueueNum;
    Qmss_QueueHnd   memoryRegionQueueHnd;
    uint8_t         isAllocated;

    /* Get the pointer to the heap. */
    ptrPktHeap = (Pktlib_Heap *)heapHandle;
    if (ptrPktHeap == NULL)
    {
        *errCode = PKTLIB_EINVAL;
        return -1; 
    }

    /* The Packet Library supports the following types of heaps:
     *  - Shared Heaps
     *  - Local Heaps
     *  - Super Heaps */
    if (ptrPktHeap->isSuperHeap == 1)
    {
        /* Super Heap is getting deleted. Super heaps are core specific so all we need
         * to do is reset the memory contents of the heap of to 0. The member heaps
         * residing below a super heap are NOT deleted. */
         memset ((void *)ptrPktHeap, 0, sizeof(Pktlib_Heap));
         return 0;
    }

    /* Sanity Check: Ensure that all the data buffers with which the heap was configured
     * are present in the free queue. Else we cannot delete the heap */
    if (ptrPktHeap->freeQueueHnd != 0)
    {
        if (Qmss_getQueueEntryCount(ptrPktHeap->freeQueueHnd) != ptrPktHeap->numDataBufferPackets)
        {
            *errCode = PKTLIB_EDATABUFFER_MISMATCH;
            return -1;
        }
    }

    /* Sanity Check: Ensure that all the zero buffer packets with which the heap was configured
     * are present in the free queue. Else we cannot delete the heap */
    if (ptrPktHeap->freeZeroQueueHnd != 0)
    {
        if (Qmss_getQueueEntryCount(ptrPktHeap->freeZeroQueueHnd) != ptrPktHeap->numZeroBufferPackets)
        {
            *errCode = PKTLIB_EZEROBUFFER_MISMATCH;
            return -1;
        }
    }

    /* Get the queue number & handle associated with the memory region. */
    memoryRegionQueueNum = Qmss_getMemRegQueueHandle(ptrPktHeap->memRegion);
    memoryRegionQueueHnd = Qmss_queueOpen (Qmss_QueueType_GENERAL_PURPOSE_QUEUE, memoryRegionQueueNum, &isAllocated);

    /* The heap has been sanity checked. Now we can delete the descriptors in the data buffer queue if there was
     * one associated with the heap. */
    if (ptrPktHeap->freeQueueHnd != 0)
    {
        while (Qmss_getQueueEntryCount(ptrPktHeap->freeQueueHnd) != 0)
        {
#ifdef _VIRTUAL_ADDR_SUPPORT
            ptrPkt = (Ti_Pkt*)QMSS_DESC_PTR(Pktlib_osalPhyToVirt(Qmss_queuePopRaw(ptrPktHeap->freeQueueHnd)));
#else
            ptrPkt = (Ti_Pkt*)QMSS_DESC_PTR(Qmss_queuePop(ptrPktHeap->freeQueueHnd));
#endif
            /* Get the original data buffer pointer & size. */
            Cppi_getOriginalBufInfo(Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt, &ptrDataBuffer, &dataBufferLen);

            /* Cleanup the memory associated with the data buffer packet */
            ptrPktHeap->heapFxnTable.data_free(ptrDataBuffer, ptrPktHeap->dataBufferSize);

            /* Place back the descriptor into the memory region free queue. */
            Qmss_queuePushDesc(memoryRegionQueueHnd, (Cppi_Desc*)ptrPkt);
        }
    }

    /* Now we can delete the descriptors in the zero data buffer queue if there was one associated with the heap. */
    if (ptrPktHeap->freeZeroQueueHnd != 0)
    {
        while (Qmss_getQueueEntryCount(ptrPktHeap->freeZeroQueueHnd) != 0)
        {
#ifdef _VIRTUAL_ADDR_SUPPORT
            ptrPkt = (Ti_Pkt*)QMSS_DESC_PTR(Pktlib_osalPhyToVirt(Qmss_queuePopRaw(ptrPktHeap->freeZeroQueueHnd)));
#else
            ptrPkt = (Ti_Pkt*)QMSS_DESC_PTR(Qmss_queuePop(ptrPktHeap->freeZeroQueueHnd));
#endif
            /* Place back the descriptor into the memory region free queue. */
            Qmss_queuePushDesc(memoryRegionQueueHnd, (Cppi_Desc*)ptrPkt);
        }
    }

    /* Now we need to close all the queues opened by the heap. 
     * - Does the heap use starvation queues? */
    if (ptrPktHeap->useStarvationQueue)
    {
        /* YES. With starvation queues; there are 4 contigious queues which 
         * are opened.  Queue0 is allocated to the data buffer queues; which
         * Queue1 is allocated to the zero buffer queue. Queue2 and Queue3 
         * are unused */
        Qmss_queueClose(ptrPktHeap->baseStarvationQueue + 2);
        Qmss_queueClose(ptrPktHeap->baseStarvationQueue + 3);

        /* Queue 0: This is also the data buffer free queue which is closed here */
        Qmss_queueClose(ptrPktHeap->baseStarvationQueue);

        /* Queue 1: This is also the data buffer free queue which is closed here */
        Qmss_queueClose(ptrPktHeap->baseStarvationQueue + 1);
    }
    else
    {
        /* NO. Starvation queues are NOT configured. Close the data buffer
         * and zero data buffer queues if configured. */
        if (ptrPktHeap->freeQueueHnd != 0)
            Qmss_queueClose(ptrPktHeap->freeQueueHnd);
    
        if (ptrPktHeap->freeZeroQueueHnd != 0)
            Qmss_queueClose(ptrPktHeap->freeZeroQueueHnd);
    }

    /* All heaps are associated with a garbage queue. */
    Qmss_queueClose(ptrPktHeap->garbageQueueHnd);

    /* Reset the contents of the heap block. */
    memset ((void *)ptrPktHeap, 0, sizeof(Pktlib_Heap));

    /* Writeback the cache contents always. */
    Pktlib_osalEndMemAccess(ptrPktHeap, sizeof(Pktlib_Heap));

    /* Heap has been successfully deleted. */
    return 0;
}

/**
@}
*/

