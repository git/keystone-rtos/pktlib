/**
 *   @file  pktlib.h
 *
 *   @brief
 *      Header file for the Packet Library. The file exposes the data structures
 *      and exported API which are available for use by applications.
 *
 *  \par
 *  NOTE:
 *      (C) Copyright 2012 Texas Instruments, Inc.
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

 
 
/** @defgroup PACKET_LIB_API Packet Library
 */
#ifndef __PKT_LIB_H__
#define __PKT_LIB_H__

/* CPPI/QMSS Include */
#include <ti/drv/qmss/qmss_drv.h>
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/cppi/cppi_desc.h>

/* 
 * Shut off: remark #880-D: parameter "descType" was never referenced
*
* This is better than removing the argument since removal would break
* backwards compatibility
*/
#ifdef _TMS320C6X
#pragma diag_suppress 880
#pragma diag_suppress 681
#elif defined(__GNUC__)
/* Same for GCC:
* warning: unused parameter descType [-Wunused-parameter]
*/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

/* Packet Library Include Files */
#include <ti/runtime/pktlib/pktlibver.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
@defgroup PKT_LIB_SYMBOL  Packet Library Defined Symbols
@ingroup PACKET_LIB_API
*/
/**
@defgroup PKT_LIB_ERROR_CODE  Packet Library Error codes
@ingroup PACKET_LIB_API
*/        
/**
@defgroup PKT_LIB_FUNCTION  Packet Library Functions
@ingroup PACKET_LIB_API
*/
/**
@defgroup PKT_LIB_DATA_STRUCTURE  Packet Library Data Structures
@ingroup PACKET_LIB_API
*/
/**
@defgroup PKTLIB_OSAL_API  Packet Library OS Abstraction Layer
@ingroup PACKET_LIB_API
*/

/** @addtogroup PKT_LIB_SYMBOL
 @{ */

/**
@}
*/

/** @addtogroup PKT_LIB_ERROR_CODE
 @{ */

/**
 * @brief   The error code indicates an invalid argument was passed to the API.
 */
#define PKTLIB_EINVAL                       -1

/**
 * @brief   This error code implies that there the MAX heap limit was exceeded.
 */
#define PKLIB_EMAXHEAPLIMIT                 -2

/**
 * @brief   This error code implies that there was a resource error. 
 */
#define PKLIB_ERESOURCE                     -3

/**
 * @brief   This error code implies that the MPU was not configured to allow the
 * PKTLIB to write to use the QMSS threshold functionality.
 */
#define PKTLIB_EPERM                        -4

/**
 * @brief   This error code implies that an OUT of memory error was detected.
 */
#define PKTLIB_ENOMEM                       -5

/**
 * @brief   The error code implies that the number of data buffers packets in the heap
 * is NOT the same as the number of the data buffer packets with which the heap was
 * created. This error is returned during heap deletion.
 */
#define PKTLIB_EDATABUFFER_MISMATCH         -6

/**
 * @brief   The error code implies that the number of zero buffers packets in the heap
 * is NOT the same as the number of the zero buffer packets with which the heap was
 * created. This error is returned during heap deletion.
 */
#define PKTLIB_EZEROBUFFER_MISMATCH         -7

/**
@}
*/
        

/** @addtogroup PKT_LIB_DATA_STRUCTURE
 @{ */

/** 
 * @brief 
 *  The packet library exposes the Ti_Pkt as an opaque handle.
 */
typedef void*   Ti_Pkt;

/** 
 * @brief 
 *  Packet Library Heap Handle
 */
typedef void*   Pktlib_HeapHandle;

/** 
 * @brief 
 *  The structure describes the Heap Interface Table
 *
 * @details
 *  There could exist multiple heaps in the system. Heaps can have different
 *  properties depending upon the memory region (cached vs. non-cached) and 
 *  how the data buffers need to be allocated. The table here provides a 
 *  well defined interface which allows this information to be registered 
 *  during heap creation. 
 */
typedef struct Pktlib_HeapIfTable
{
    /**
     * @brief   This API will be called by the packet library during heap creation 
     * to allocate data memory for the packets.
     */
    uint8_t* (*data_malloc)(uint32_t size);

    /**
     * @brief   This API will be called by the packet library during heap deletion
     * or resizing to clean up data memory for the packets.
     */
    void     (*data_free)(uint8_t* ptrDataBuffer, uint32_t size);
}Pktlib_HeapIfTable;

/**
 * @brief 
 *  The structure describes the Heap Configuration.
 *
 * @details
 *  The configuration is populated and passed to the PKTLIB module when a heap
 *  is being created. 
 */
typedef struct Pktlib_HeapCfg
{
    /**
     * @brief   Heap Name which should be unique and identifies the heap.
     */
    const char*         name;

    /**
     * @brief   QMSS memory region from where the packets will be carved out
     */
    Qmss_MemRegion      memRegion;

    /**
     * @brief   This flag identifies if the heap is shared or private. Private
     * heaps are visible only on the core while shared heaps are visible across
     * cores.
     */
    uint32_t             sharedHeap;

    /**
     * @brief   This flag indicates that the heap should use STARVATION queues
     * which allow the Navigator infrastructure to detect queue empty and record
     * statistics. 
     */
    uint32_t             useStarvationQueue;

    /**
     * @brief   The threshold can be used to determine heap buffer usage. If the
     * data buffer packets in the heap fall below the threshold value this will
     * be detected. Please ensure that the thresholds are always a power of 2
     */
    uint32_t            dataBufferPktThreshold;

    /**
     * @brief   The threshold can be used to determine heap zero-buffer usage. If 
     * the zero buffer packets in the heap fall below the threshold value this will
     * be detected. Please ensure that the thresholds are always a power of 2
     */
    uint32_t            zeroBufferPktThreshold;

    /**
     * @brief   Each heap has data buffers of the size specified here.
     */
    uint32_t            dataBufferSize;

    /**
     * @brief   These are the number of packets which are associated with the
     * data buffer size specified above.
     */
    uint32_t            numPkts;

    /**
     * @brief   These are the number of zero buffer packets which should be 
     * present in the heap
     */
    uint32_t            numZeroBufferPackets;

    /**
     * @brief   Heap Interface Function table which identifies functions for 
     * data allocation & cleanup.
     */
    Pktlib_HeapIfTable  heapInterfaceTable;
}Pktlib_HeapCfg;

/**
 * @brief 
 *  The structure describes the Heap Statistics
 *
 * @details
 *  Heap statistics reported by the packet library
 */
typedef struct Pktlib_HeapStats
{
    /**
     * @brief   This is the current number of free data packets 
     * which are available
     */
    uint32_t    numFreeDataPackets;

    /**
     * @brief   This is the current number of free packets (with no buffers) 
     * which are available
     */
    uint32_t    numZeroBufferPackets;

    /**
     * @brief   This is the current number of packets (with & without) data buffers 
     * which are in the garbage queue.
     */
    uint32_t    numPacketsinGarbage;
    
    /**
     * @brief   This field is set to indicate that the heap has hit the specified 
     * data buffer threshold. It is applicable only if the 'starvation data threshold'
     * was specified during heap creation.
     */
    uint8_t    dataBufferThresholdStatus;

    /**
     * @brief   This is set only if the heap was configured to use starvation queues
     * and indicates the number of times the data buffer queue was starved
     * of packets.
     */
    uint8_t     dataBufferStarvationCounter;

    /**
     * @brief   This field is set to indicate that the heap has hit the specified 
     * zero data buffer threshold.It is applicable only if the 'starvation zero buffer
     * treshold' was specified during heap creation. 
     */
    uint8_t     zeroDataBufferThresholdStatus;

    /**
     * @brief   This is set only if the heap was configured to use starvation queues
     * and indicates the number of times the zero buffer queue was starved of packets.
     */
    uint8_t     zeroDataBufferStarvationCounter;
}Pktlib_HeapStats;

/**
@}
*/

/** @addtogroup PKT_LIB_FUNCTION
 @{ */

/**
 *  @b Description
 *  @n  
 *      The function is used to get the descriptor from the TI packet.
 *
 *  @param[in]  ptrPkt
 *      Pointer to the TI packet
 *
 *  @retval
 *      Pointer to the host descriptor.
 */
static inline Cppi_HostDesc* Pktlib_getDescFromPacket(Ti_Pkt* ptrPkt)
{
    return (Cppi_HostDesc*)ptrPkt;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the TI packet from the descriptor
 *
 *  @param[in]  ptrHostDesc
 *      Pointer to the host descriptor.
 *
 *  @retval
 *      Pointer to the TI packet.
 */
static inline Ti_Pkt* Pktlib_getPacketFromDesc(Cppi_HostDesc* ptrHostDesc)
{
    return (Ti_Pkt*)ptrHostDesc;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the number of descriptors which are a part of the
 *      packet.
 *
 *  @param[in]  ptrPkt
 *      Packet for which the number of descriptors are required.
 *
 *  @retval
 *      Number of buffers.
 */
static inline uint8_t Pktlib_packetBufferCount(Ti_Pkt* ptrPkt)
{
    Cppi_HostDesc*  ptrDesc = Pktlib_getDescFromPacket(ptrPkt);
    uint8_t         count = 0;

    if (ptrDesc == NULL)
        return count;

    /* Cycle through all the descriptors in the packet. */
    do
    {
        /* Increment the number of the descriptors which are available. */
        count = count + 1;

        /* Get the next descriptor. */
        ptrDesc = (Cppi_HostDesc*)Cppi_getNextBD(Cppi_DescType_HOST, (Cppi_Desc *)ptrDesc);
    }while (ptrDesc != NULL);

    /* Return the number of buffers. */
    return count;
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the data buffer associated with the packet.
 *
 *  @param[in]  ptrPkt
 *      Pointer to the packet
 *  @param[in]  ptrDataBuffer
 *      Data Buffer associated with the packet populated by this API
 *  @param[in]  dataLen
 *      Data Buffer Length populated by this API
 *
 *  @retval
 *     Not Applicable. 
 */
static inline void Pktlib_getDataBuffer(Ti_Pkt* ptrPkt, uint8_t** ptrDataBuffer, uint32_t* dataLen)
{
    Cppi_getData (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt, ptrDataBuffer, dataLen);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the length of the data buffer associated with the packet.
 *
 *  @param[in]  ptrPkt
 *      Pointer to the packet
 *
 *  @retval
 *      Data Buffer associated with the packet.
 */
static inline uint32_t Pktlib_getDataBufferLen(Ti_Pkt* ptrPkt)
{
    return Cppi_getDataLen (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the length of the packet. The function 
 *      should *only* be called for packets which have a single node or 
 *      for the head of the chained list of packets. Using this API for any 
 *      other packet is not supported and would cause issues especially 
 *      if these packets are being pushed across peripherals.
 *
 *  @param[in]  ptrPkt
 *      Pointer to the packet
 *
 *  @retval
 *      Length of the packet.
 */
static inline uint32_t Pktlib_getPacketLen(Ti_Pkt* ptrPkt)
{
    return Cppi_getPacketLen (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to get the next packet 
 *
 *  @param[in]  ptrPkt
 *      Pointer to the packet
 *
 *  @retval
 *      Next Linked Packet.
 */
static inline Ti_Pkt* Pktlib_getNextPacket(Ti_Pkt* ptrPkt)
{
    return (Ti_Pkt*)Cppi_getNextBD(Cppi_DescType_HOST, (Cppi_Desc *)ptrPkt);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to set the data buffer length associated with the packet.
 *
 *  @param[in]  ptrPkt
 *      Pointer to the packet
 *  @param[in]  dataLen
 *      Data Buffer Length to be configured
 *
 *  @retval
 *     Not Applicable. 
 */
static inline void Pktlib_setDataBufferLen(Ti_Pkt* ptrPkt, uint32_t dataLen)
{
    Cppi_setDataLen (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt, dataLen);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to set the length of the packet. The function 
 *      should *only* be called for packets which have a single node or 
 *      for the head of the chained list of packets. Using this API for any 
 *      other packet is not supported and would cause issues especially 
 *      if these packets are being pushed across peripherals.
 *
 *  @param[in]  ptrPkt
 *      Pointer to the packet
 *  @param[in]  packetLen
 *      Length of the packet to be configured.
 *
 *  @retval
 *      Not applicable
 */
static inline void Pktlib_setPacketLen(Ti_Pkt* ptrPkt, uint32_t packetLen)
{
    Cppi_setPacketLen (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt, packetLen);
}

/**
 *  @b Description
 *  @n  
 *      The function is used to offset the data buffer in the packet by a specified
 *      number of bytes. The data buffer length is also modified to account for the
 *      offset.
 *
 *  @param[in]  ptrPkt
 *      Pointer to the packet
 *  @param[in]  offset
 *      Offset value in bytes.
 *
 *  @retval
 *      Success -   0
 *  @retval
 *      Error   -   <0
 */
static inline int32_t Pktlib_setDataOffset(Ti_Pkt* ptrPkt, int32_t offset)
{
    uint8_t*    ptrDataBuffer;
    uint32_t    dataLen;
    uint8_t*    ptrOrigDataBuffer;
    uint32_t    origDataLen;    

    /* Get the current data information. */
    Cppi_getData (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt,
                  (uint8_t**)&ptrDataBuffer, &dataLen);

    /* Sanity Check: Is the offset +ve or -ve? */
    if (offset >= 0)
    {
        /* +ve: Make sure that the offset does not exceed the current data length. */
        if (offset > (int32_t)dataLen)
            return -1;
    }
    else
    {
        /* -ve: Get the original buffer information. */
        Cppi_getOriginalBufInfo (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt,
                                 (uint8_t**)&ptrOrigDataBuffer, &origDataLen);

        /* Make sure we dont go back beyond the original data buffer. */
        if ((dataLen - offset) > origDataLen)
            return -1;
    }

    /* Offset the data and account for it in the data length also. */
    Cppi_setData (Cppi_DescType_HOST, (Cppi_Desc*)ptrPkt, (uint8_t*)(ptrDataBuffer + offset), 
                  (dataLen - offset));

    /* Data was offset successfully.*/
    return 0;
}

/**
 *  @b Description
 *  @n  
 *      The PKTLIB uses the SOURCE_TAG_HI field to store its private information. 
 *      This field can get overwritten using the native Cppi_setTag API. This API 
 *      is a replacement of the above API because it ensures that the PKTLIB 
 *      Private information is always preserved. 
 *
 *  @param[in]  ptrPkt
 *      Pointer to the packet
 *  @param[in]  tag
 *      Tags to be configured in the packet.
 *
 *  @retval
 *      Not applicable
 */
static inline void Pktlib_setTags(Ti_Pkt* ptrPkt, Cppi_DescTag* tag)
{
    uint8_t         pktlibPrivateInfo;
    Cppi_HostDesc*  ptrHostDesc;

    /* Get the host descriptor. */
    ptrHostDesc = Pktlib_getDescFromPacket(ptrPkt);

    /* Get the PKTLIB Private information. */
    pktlibPrivateInfo = CSL_FEXTR(ptrHostDesc->tagInfo, 31, 24);

    /* Store the new tag information into the packet. */
    ptrHostDesc->tagInfo = CSL_FMKR(7,  0,  tag->destTagLo)     |
                           CSL_FMKR(15, 8,  tag->destTagHi)     |
                           CSL_FMKR(23, 16, tag->srcTagLo)      |
                           CSL_FMKR(31, 24, pktlibPrivateInfo);
}

/**
@}
*/

/**********************************************************************
 **************************** EXPORTED API ****************************
 **********************************************************************/

extern Ti_Pkt* Pktlib_packetMerge(Ti_Pkt* pPkt1, Ti_Pkt* pPkt2, Ti_Pkt* pLastPkt);
extern int32_t Pktlib_clonePacket(Ti_Pkt* ptrPktOrig, Ti_Pkt* ptrClonePacket);
extern Ti_Pkt* Pktlib_allocPacket(Pktlib_HeapHandle heapHandle, uint32_t size);
extern void Pktlib_freePacket(Ti_Pkt* pPkt);
extern int32_t Pktlib_splitPacket(Ti_Pkt* pOrgPkt, Ti_Pkt* pNewPkt, 
                                  uint32_t splitPacketSize, 
                                  Ti_Pkt** pPkt1,
                                  Ti_Pkt** pPkt2);
extern int32_t Pktlib_splitPacket2(Ti_Pkt* pOrgPkt, Ti_Pkt* pNewPkt, 
                                  uint32_t splitPacketSize, 
                                  Ti_Pkt** pPkt1,
                                  Ti_Pkt** pPkt2);
extern Pktlib_HeapHandle Pktlib_createHeap(Pktlib_HeapCfg* ptrHeapCfg,int32_t* errCode);
extern int32_t Pktlib_deleteHeap (Pktlib_HeapHandle heapHandle, int32_t* errCode);
extern Pktlib_HeapHandle Pktlib_findHeapByName (const char* heapName);
extern void Pktlib_getHeapStats(Pktlib_HeapHandle heapHandle, Pktlib_HeapStats* ptrHeapStats);
extern Pktlib_HeapHandle Pktlib_getPktHeap(Ti_Pkt* pPkt);
extern void Pktlib_garbageCollection(Pktlib_HeapHandle heapHandle);

extern int32_t Pktlib_sharedHeapInit (void);
extern void Pktlib_invalidatePkt(Ti_Pkt* pPkt);
extern void Pktlib_writebackPkt(Ti_Pkt* pPkt);

extern Qmss_QueueHnd Pktlib_getZeroHeapQueue(Pktlib_HeapHandle heapHandle);
extern Qmss_QueueHnd Pktlib_getInternalHeapQueue(Pktlib_HeapHandle heapHandle);

extern uint32_t Pktlib_getMaxBufferSize(Pktlib_HeapHandle heapHandle);

extern Pktlib_HeapHandle Pktlib_createSuperHeap(const char* heapName,
                                                Pktlib_HeapHandle  memberHeap[], 
                                                int32_t     numMemberHeaps,
                                                int32_t*    errCode);

#ifdef __cplusplus
}
#endif

#endif /* __PKT_LIB_H__ */

