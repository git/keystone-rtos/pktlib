/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the Packet Library
 *
 * Copyright (C) 2009-2018 Texas Instruments, Inc.
 *****************************************************************************/

package ti.runtime.pktlib[2, 1, 0, 8] {
}

