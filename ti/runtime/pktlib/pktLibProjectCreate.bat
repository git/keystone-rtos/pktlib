@REM ******************************************************************************
@REM * FILE PURPOSE: PKTLIB Unit Test and Example Project Creator
@REM ******************************************************************************
@REM * FILE NAME: pktlibProjectCreate.bat
@REM *
@REM * DESCRIPTION: 
@REM *  The script file is used to create the test and example projects of all
@REM *  components under PKTLIB. These projects are available in the specified 
@REM *  workspace.
@REM *
@REM * USAGE:
@REM *  pktlibProjectCreate.bat 
@REM *      --- OR ---
@REM * Description:     (first option is default)
@REM *  socFamily  -   k1/k2k / k2h
@REM *  endian      -   little / big
@REM *
@REM *
@REM * Copyright (C) 2010, Texas Instruments, Inc.
@REM *****************************************************************************

@echo OFF

REM Parameter Validation: Check if the argument was passed to the batch file and
REM *****************************************************************************
REM Argument [socFamily] is used to set DEVICE_NAME variable.
REM Valid values are  'k2h'. Defaults to 'none Keystone-1'.
set tempVar1=%1
if not defined tempVar1 goto nodevice
set DEVICE_NAME=%tempVar1%
goto devicedone
:nodevice
set DEVICE_NAME=
:devicedone
REM otherwise the batch file commands do not work 
set tempVar=%1
IF NOT DEFINED tempVar GOTO noparameter
set PKTLIB_SHORT_NAME=%~fs1
goto done
:noparameter
set PKTLIB_SHORT_NAME=%~sdp0
:done
REM *****************************************************************************

echo =========================================================================
echo.   DEVICE_NAME     :   %DEVICE_NAME%
echo.   ENDIAN          :   %ENDIAN%
echo =========================================================================

REM *****************************************************************************
REM * Version Information of the various tools etc required to build the test
REM * projects. Customers are free to modify these to meet their requirements.
REM *****************************************************************************

REM This is to control the CCS version specific project create command
REM Set to 'no' when using CCSv5 or set to 'yes' when using CCSv4
set IS_CCS_VERSION_4=no

REM Set to 'no' when using QT, EVM, VDB, or other hardware. Set to 'yes' only when using the simulator.
set IS_SIMULATOR_SUPPORT_NEEDED=no

REM Install Location for CCS
REM set CCS_INSTALL_PATH="C:\Program Files\Texas Instruments\ccsv5"
REM set CCS_INSTALL_PATH="C:\Program Files\Texas Instruments\ccsv4"
set CCS_INSTALL_PATH="C:\ti\ccs_5_3_0\ccsv5"

REM Workspace where the PKTLIB projects will be created.
set MY_WORKSPACE="C:\MyPKTLIBWorkspacek2hLE"

REM macros.ini location
set MACROS_FILE=macros.ini

REM This is Endianess of the Projects being created.
REM Valid Values are 'little' and 'big'
REM set ENDIAN=big
set ENDIAN=little

REM This is the format of the executable being created
REM Valid Values are 'ELF' and 'COFF'
set OUTPUT_FORMAT=ELF

REM Version of CG-Tools
set CGT_VERSION=7.4.2

REM Version of XDC
set XDC_VERSION=3.23.03.53

REM Version of BIOS
set BIOS_VERSION=6.33.06.50

REM Version of the IPC
set IPC_VERSION=1.24.03.32

REM EDMA3 Version 
set EDMA_VERSION=02.11.05

REM Version of the PDK
set PDK_VERSION=1.00.00.05

REM PDK Part Number
set PDK_PARTNO=TCI6634
REM set PDK_PARTNO=C6678L

REM RTSC Platform Name
set RTSC_PLATFORM_NAME=ti.platforms.simKepler

REM RTSC Target 
REM - Please ensure that you select this taking into account the
REM   OUTPUT_FORMAT and the RTSC_PLATFORM_NAME 
REM set RTSC_TARGET=ti.targets.elf.C66_big_endian
set RTSC_TARGET=ti.targets.elf.C66

REM *****************************************************************************
REM *****************************************************************************
REM                 Please do NOT change anything below this
REM *****************************************************************************
REM *****************************************************************************

REM Set auto create command by default for use with CCSv5
set AUTO_CREATE_COMMAND=eclipse\eclipsec -noSplash

REM If is CCS version 4 then set auto create command for use with CCSv4
If .%IS_CCS_VERSION_4% == .yes set AUTO_CREATE_COMMAND=eclipse\jre\bin\java -jar %CCS_INSTALL_PATH%\eclipse\startup.jar

REM Set project for Silicon or QT by default
set SIMULATOR_SUPPORT_DEFINE=

REM If simulator support is needed then set the define
If .%IS_SIMULATOR_SUPPORT_NEEDED% == .yes set SIMULATOR_SUPPORT_DEFINE=-ccs.setCompilerOptions "--define SIMULATOR_SUPPORT"

REM Goto the PKTLIB Installation Path.
pushd %PKTLIB_SHORT_NAME%

echo *****************************************************************************
echo Detecting UnitTest Projects in PKTLIB and importing them in the workspace %MY_WORKSPACE%

REM Search for all the test Project Files in the PKTLIB.
for /F %%I IN ('dir /b /s *%DEVICE_NAME%*testproject.txt') do (
echo Detected Test Project: %%~nI

REM Goto each directory where the test project file is located and create the projects.
pushd %%~dI%%~pI

REM Execute the command to create the project using the parameters specified above.
%CCS_INSTALL_PATH%\%AUTO_CREATE_COMMAND% -data %MY_WORKSPACE% -application com.ti.ccstudio.apps.projectCreate -ccs.name %%~nI -ccs.outputFormat %OUTPUT_FORMAT% -ccs.device com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice -ccs.endianness %ENDIAN% -ccs.kind executable -ccs.cgtVersion %CGT_VERSION% -rtsc.xdcVersion %XDC_VERSION% -rtsc.enableDspBios -rtsc.biosVersion %BIOS_VERSION% -rtsc.buildProfile "debug" -rtsc.products "com.ti.sdo.edma3:%EDMA_VERSION%;com.ti.rtsc.IPC:%IPC_VERSION%;com.ti.rtsc.SYSBIOS:%BIOS_VERSION%;ti.pdk:%PDK_VERSION%" -rtsc.platform "%RTSC_PLATFORM_NAME%" -rtsc.target %RTSC_TARGET% -ccs.rts libc.a -ccs.args %%~nI%%~xI %SIMULATOR_SUPPORT_DEFINE% -ccs.setCompilerOptions "--define DEVICE_K2H"
echo Copying macro.ini
copy %MACROS_FILE% %MY_WORKSPACE%\%%~nI\macros.ini
popd
)

echo *****************************************************************************
echo Detecting Example Projects in PKTLIB and importing them in the workspace %MY_WORKSPACE%

REM Search for all the Example Project Files in the PKTLIB.
for /F %%I IN ('dir /b /s *exampleproject.txt') do (
echo Detected Example Project: %%~nI

REM Goto each directory where the example project file is located and create the projects.
pushd %%~dI%%~pI

REM Execute the command to create the project using the parameters specified above.
%CCS_INSTALL_PATH%\%AUTO_CREATE_COMMAND% -data %MY_WORKSPACE% -application com.ti.ccstudio.apps.projectCreate -ccs.name %%~nI -ccs.outputFormat %OUTPUT_FORMAT% -ccs.device com.ti.ccstudio.deviceModel.C6000.GenericC64xPlusDevice -ccs.endianness %ENDIAN% -ccs.kind executable -ccs.cgtVersion %CGT_VERSION% -rtsc.xdcVersion %XDC_VERSION% -rtsc.enableDspBios -rtsc.biosVersion %BIOS_VERSION% -rtsc.buildProfile "debug" -rtsc.products "com.ti.rtsc.IPC:%IPC_VERSION%;com.ti.rtsc.SYSBIOS:%BIOS_VERSION%;com.ti.biosmcsdk.pdk.%PDK_PARTNO%:%PDK_VERSION%" -rtsc.platform "%RTSC_PLATFORM_NAME%" -rtsc.target %RTSC_TARGET% -ccs.rts libc.a -ccs.args %%~nI%%~xI %SIMULATOR_SUPPORT_DEFINE% -ccs.setBuildOption com.ti.rtsc.*.XDC_PATH "${PKTLIB_INSTALL_PATH}" -ccs.setBuildOption com.ti.rtsc.*.XDC_PATH "${PKTLIB_INSTALL_PATH}"
echo Copying macro.ini
copy %MACROS_FILE% %MY_WORKSPACE%\%%~nI\macros.ini
popd
)

popd

